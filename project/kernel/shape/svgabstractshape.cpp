#include "kernel/shape/svgabstractshape.h"

Shape::SvgAbstractShape::SvgAbstractShape():m_borderColor(QColor("#000000"))
	,m_backgroundColor(QColor("#ffffff"))
	,m_borderWidth(2)
	,m_borderstyle()
	,m_Opacity(0)
	,m_blur(0)
	,m_ratotation(0)
	, isDrawend(false)
{
	
}

Shape::SvgAbstractShape::~SvgAbstractShape()
{

}

int Shape::SvgAbstractShape::getRatotion() const
{
	return m_ratotation;
}

int Shape::SvgAbstractShape::getOpacity() const
{
	return m_Opacity;
}

int Shape::SvgAbstractShape::getBlur() const
{
	return m_blur;
}

int Shape::SvgAbstractShape::getBorderWidth() const
{
	return m_borderWidth;
}

void Shape::SvgAbstractShape::setBorderColor(const QColor&c)
{
	m_borderColor = c;
}

void Shape::SvgAbstractShape::setBackgroundColor(const QColor&c)
{
	m_backgroundColor = c;
}

QColor Shape::SvgAbstractShape::getBorderColor() const
{
	return m_borderColor;
}

QColor Shape::SvgAbstractShape::getBackgroundColor() const
{
	return m_backgroundColor;
}

void Shape::SvgAbstractShape::setBorderStyle(const BorderLineStyle& borderstyle)
{
	m_borderstyle = borderstyle;
}

void Shape::SvgAbstractShape::setRatotion(const int& rat)
{
	m_ratotation = rat;
	if (360 <= m_ratotation) {
		m_ratotation -= 360;
	}
	else if(-360>=m_ratotation)
 	{
		m_ratotation += 360;
	}
}

void Shape::SvgAbstractShape::setOpacity(const int& value)
{
	m_Opacity = value;
	this->m_backgroundColor.setAlpha(m_Opacity);
	this->m_borderColor.setAlpha(m_Opacity);
}

void Shape::SvgAbstractShape::setBlur(const int& blur)
{
	m_blur = blur;
}

void Shape::SvgAbstractShape::setBorderWidth(const int& borderwidth)
{
	m_borderWidth = borderwidth;
}



void Shape::SvgAbstractShape::setPenStyle(const Qt::PenStyle style)
{

}

void Shape::SvgAbstractShape::moveTo(const QPointF&pos)
{
	int size = m_drawPath.size();
	for (auto& i : m_drawPath) {
		i = QPointF(i.x()+ pos.x(),i.y()+pos.y());
	}
}

void Shape::SvgAbstractShape::moveTo(const double& x, const double& y)
{
	int size = m_drawPath.size();
	for (int i = 0; i < size; ++i)
	{
		m_drawPath[i] = QPointF(m_drawPath[i].x() + x, m_drawPath[i].y() + y);
	}
}

Shape::ShapeType Shape::SvgAbstractShape::getShapeType() const
{
	return m_shapeType;
}

void Shape::SvgAbstractShape::setShapeType(const ShapeType& shapetype)
{
	m_shapeType = shapetype;
}

void Shape::SvgAbstractShape::setStartPosition(const QPointF&pos)
{
	m_stPos = pos;
}

void Shape::SvgAbstractShape::setEndPosition(const QPointF&pos)
{
	m_enPos = pos;
}

QPointF Shape::SvgAbstractShape::getStartPosition() const
{
	return m_stPos;
}

QPointF Shape::SvgAbstractShape::getEndPosition() const
{
	return m_enPos;
}

void Shape::SvgAbstractShape::setDrawend(const bool flags)
{
	this->isDrawend = flags;
}

bool Shape::SvgAbstractShape::getDrawend() const
{
	return this->isDrawend;
}

void Shape::SvgAbstractShape::setPath(const QVector<QPointF>&path)
{
	m_drawPath = path;
}

QVector<QPointF> Shape::SvgAbstractShape::getPath() const
{
	return m_drawPath;
}

QRectF Shape::SvgAbstractShape::getRectF() const
{
	if (2 > m_drawPath.size()) return QRectF();
	double minX = m_drawPath[0].x(), minY = m_drawPath[0].y(),
		maxX = m_drawPath[0].x(), maxY = m_drawPath[0].y();
	for (auto i:m_drawPath)
	{
		minX = i.x() < minX ? i.x() : minX;
		minY = i.y() < minY ? i.y() : minY;
		maxX = i.x() > maxX ? i.x() : maxX;
		maxY = i.y() > maxY ? i.y() : maxY;
	}
	return QRectF(QPointF(minX, minY), QPointF(maxX, maxY));
}

int Shape::SvgAbstractShape::getPositionX() const
{
	return getRectF().topLeft().x();
}

int Shape::SvgAbstractShape::getPositionY() const
{
	return getRectF().topLeft().y();

}

int Shape::SvgAbstractShape::getRectW() const
{
	return getRectF().width();

}

int Shape::SvgAbstractShape::getRectH() const
{
	return getRectF().height();

}

QPointF Shape::SvgAbstractShape::getCenterPointF() const
{
	return getRectF().center();
}
