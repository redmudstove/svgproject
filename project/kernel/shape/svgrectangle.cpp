#include "kernel/shape/svgrectangle.h"
#include <QPainterPath>
#include <QPainter>
#include <QPen>
#include <QBrush>
Shape::SvgRectangle::SvgRectangle()
{
	m_shapeType = Shape::ShapeType::rectangle;
}

Shape::SvgRectangle::~SvgRectangle()
{

}

bool Shape::SvgRectangle::isSelect() const
{
	return true;
}


bool Shape::SvgRectangle::isContains(const QPointF& pos) const
{
	QPointF clickp = pos;
	if (0 != m_ratotation)
	{
		double x = (pos.x() - getCenterPointF().x()) * cos(-m_ratotation / 180 * PI)
			- (pos.y() - getCenterPointF().y()) * sin(-m_ratotation / 180 * PI)
			+ getCenterPointF().x();
		double y = (pos.x() - getCenterPointF().x()) * sin(-m_ratotation / 180 * PI)
			+ (pos.y() - getCenterPointF().y()) * cos(-m_ratotation / 180 * PI)
			+ getCenterPointF().y();
		clickp = QPointF(x, y);
	}
	QPainterPath path;
	path.addRect(QRectF(m_drawPath[0], m_drawPath[1]).normalized());
	if (path.controlPointRect().contains(clickp))
		return true;
	double length1
		= sqrt
		(pow(abs(m_drawPath[0].x() - clickp.x()), 2) +
			pow(abs(m_drawPath[0].y() - clickp.y()), 2));
	double length2
		= sqrt
		(pow(abs(m_drawPath[1].x() - clickp.x()), 2) +
			pow(abs(m_drawPath[1].y() - clickp.y()), 2));
	double length
		= sqrt
		(pow(abs(m_drawPath[0].x() - m_drawPath[1].x()), 2) +
			pow(abs(m_drawPath[0].y() - m_drawPath[1].y()), 2));
	return ((length1 + length2) <= length * 1.005) ? true : false;
}

bool Shape::SvgRectangle::isContains(const double& x, const double& y) const
{
	return isContains(QPointF(x, y));
}

void Shape::SvgRectangle::zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP)
{
	double dx = getRectF().width();
	double dy = getRectF().height();
	double mx = scaleX;
	double my = scaleY;
	if ((dx <= 5 && scaleX < 0) || (dy <= 5 && scaleY < 0))
		return;
	if (dx + scaleX < 5)
		mx = 5 - dx;
	if (dy + scaleY < 5)
		my = 5 - dy;
	for (int i = 0; i < m_drawPath.size(); i++)
	{
		if (dx != 0 && dy != 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx * (m_drawPath[i].x() - centerP.x()) / dx
				, m_drawPath[i].y() + my * (m_drawPath[i].y() - centerP.y()) / dy);
		else if (dx != 0 && dy == 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx * (m_drawPath[i].x() - centerP.x()) / dx
				, m_drawPath[i].y());
		else if (dx == 0 && dy != 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x()
				, m_drawPath[i].y() + my * (m_drawPath[i].y() - centerP.y()) / dy);
		else
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx
				, m_drawPath[i].y() + my);
	}
}

void Shape::SvgRectangle::drawShape(QPainter* painter, const double& m_scaling)
{
	if (m_drawPath.size() < 2) {

		return;
	}
	painter->save();
	QPen pen;
	pen.setWidth(m_borderWidth);
	pen.setColor(m_borderColor);
	QBrush brush;
	brush.setColor(m_backgroundColor);
	brush.setStyle(Qt::SolidPattern);
	painter->setPen(pen);
	painter->setBrush(brush);
	QPointF newStartPos;
	QPointF newEndPos;
	if (0 != m_ratotation)
	{
		newStartPos = (m_drawPath[0] - getCenterPointF()) * m_scaling;
		newEndPos = (m_drawPath[1] - getCenterPointF()) * m_scaling;
		painter->translate(QPointF(getCenterPointF() * m_scaling));
		painter->rotate(m_ratotation);
	}
	else
	{
		newStartPos = m_drawPath[0] * m_scaling;
		newEndPos = m_drawPath[1] * m_scaling;
	}
	painter->drawRect(QRectF(newStartPos, newEndPos));
	painter->restore();
}

