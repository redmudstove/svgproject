#include "shape/svgline.h"
#include <QPainter>
#include <QLine>
#include <QDebug>
Shape::SvgLine::SvgLine():SvgAbstractShape()
{
	m_shapeType = Shape::ShapeType::line;
}

Shape::SvgLine::~SvgLine()
{

}

bool Shape::SvgLine::isSelect() const
{
	return true;
}

bool Shape::SvgLine::isContains(const QPointF& pos) const
{
	QPointF clickp = pos;
	if (0 != m_ratotation)
	{
		double x = (pos.x() - getCenterPointF().x()) * cos(-m_ratotation / 180 * PI)
			- (pos.y() - getCenterPointF().y()) * sin(-m_ratotation / 180 * PI)
			+ getCenterPointF().x();
		double y = (pos.x() - getCenterPointF().x()) * sin(-m_ratotation / 180 * PI)
			+ (pos.y() - getCenterPointF().y()) * cos(-m_ratotation / 180 * PI)
			+ getCenterPointF().y();
		clickp = QPointF(x, y);
	}
	double length1
		= sqrt
		(pow(abs(m_drawPath[0].x() - clickp.x()), 2) +
			pow(abs(m_drawPath[0].y() - clickp.y()), 2));
	if (5 >= length1)
		return true;
	double length2
		= sqrt
		(pow(abs(m_drawPath[1].x() - clickp.x()), 2) +
			pow(abs(m_drawPath[1].y() - clickp.y()), 2));
	double length
		= sqrt
		(pow(abs(m_drawPath[0].x() - m_drawPath[1].x()), 2) +
			pow(abs(m_drawPath[0].y() - m_drawPath[1].y()), 2));
	return ((length1 + length2) <= length * 1.005) ? true : false;
}

bool Shape::SvgLine::isContains(const double& x, const double& y) const
{
	return isContains(QPointF(x, y));
}

void Shape::SvgLine::drawShape(QPainter* painter, const double& m_scaling)
{
	//painter->save();
	//painter->setRenderHint(QPainter::Antialiasing);
	//QPen pen;
	//pen.setWidth(m_borderWidth);
	//pen.setColor(m_borderColor);
	//QBrush brush;
	//brush.setStyle(Qt::SolidPattern);
	//brush.setColor(m_backgroundColor);
	//painter->setPen(pen);
	//painter->setBrush(brush);
	////QLine line(m_stPos.x(),m_s/*t*/Pos.y(),m_endPos.x(),m_endPos.y());
	//painter->drawLine(m_stPos,m_enPos);
	//painter->restore();

	if (m_drawPath.size() < 2)
		return;
	painter->save();
	QPen pen;
	pen.setWidth(m_borderWidth);
	pen.setColor(m_borderColor);
	pen.setStyle(Qt::SolidLine);
	QPointF newStartPos;
	QPointF newEndPos;
	painter->setPen(pen);
	if (0 != m_ratotation) {
		newStartPos = (m_drawPath[0] - getCenterPointF()) * m_scaling;
		newEndPos = (m_drawPath[1] - getCenterPointF()) * m_scaling;
		painter->rotate(m_ratotation);
	}
	else
	{
		newStartPos = m_drawPath[0] * m_scaling;
		newEndPos = m_drawPath[1] * m_scaling;
	}
	painter->drawLine(newStartPos, newEndPos);
	painter->restore();
	

}

void Shape::SvgLine::moveTo(const QPointF&)
{

}


void Shape::SvgLine::setPosition(const QPointF&, const QPointF&)
{

}

void Shape::SvgLine::zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP)
{
	double dx = getRectF().width();
	double dy = getRectF().height();
	double mx = scaleX;
	double my = scaleY;
	if ((dx <= 5 && scaleX < 0) || (dy <= 5 && scaleY < 0))
		return;
	if (dx + scaleX < 5)
		mx = 5 - dx;
	if (dy + scaleY < 5)
		my = 5 - dy;
	for (int i = 0; i < m_drawPath.size(); i++)
	{
		if (dx != 0 && dy != 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx * (m_drawPath[i].x() - centerP.x()) / dx
				, m_drawPath[i].y() + my * (m_drawPath[i].y() - centerP.y()) / dy);
		else if (dx != 0 && dy == 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx * (m_drawPath[i].x() - centerP.x()) / dx
				, m_drawPath[i].y());
		else if (dx == 0 && dy != 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x()
				, m_drawPath[i].y() + my * (m_drawPath[i].y() - centerP.y()) / dy);
		else
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx
				, m_drawPath[i].y() + my);
	}
}



