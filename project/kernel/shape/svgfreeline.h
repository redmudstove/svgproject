#ifndef SVGFREELINE_H_
#define SVGFREELINE_H_
#include "kernel/shape/svgabstractshape.h"
namespace Shape 
{
	class SvgFreeLine:public SvgAbstractShape
	{

	public:
		SvgFreeLine();
		~SvgFreeLine();
		bool isSelect() const override;
		bool isContains(const double& x, const double& y) const override;
		bool isContains(const QPointF& pos) const override;
		void zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP) override;
		void drawShape(QPainter* painter, const double& m_scaling) override;
	protected:
	private:
	};


}
#endif
