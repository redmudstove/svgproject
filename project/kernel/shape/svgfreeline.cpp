#include "kernel/shape/svgfreeline.h"
#include <QPolygonF>
#include <QPainterPath>
#include <QPainter>
#include <QPen>
#include <QBrush>
Shape::SvgFreeLine::SvgFreeLine()
{
	m_shapeType = ShapeType::freeLine;
}

Shape::SvgFreeLine::~SvgFreeLine()
{

}

bool Shape::SvgFreeLine::isSelect() const
{
	return true;
}

bool Shape::SvgFreeLine::isContains(const double& x, const double& y) const
{
	return isContains(QPointF(x, y));
}

void Shape::SvgFreeLine::zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP)
{
	double dx = getRectF().width();
	double dy = getRectF().height();
	double mx = scaleX;
	double my = scaleY;
	if ((dx <= 5 && scaleX < 0) || (dy <= 5 && scaleY < 0))
		return;
	if (dx + scaleX < 5)
		mx = 5 - dx;
	if (dy + scaleY < 5)
		my = 5 - dy;
	for (int i = 0; i < m_drawPath.size(); i++)
	{
		if (dx != 0 && dy != 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx * (m_drawPath[i].x() - centerP.x()) / dx
				, m_drawPath[i].y() + my * (m_drawPath[i].y() - centerP.y()) / dy);
		else if (dx != 0 && dy == 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx * (m_drawPath[i].x() - centerP.x()) / dx
				, m_drawPath[i].y() + my);
		else if (dx == 0 && dy != 0)
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx
				, m_drawPath[i].y() + my * (m_drawPath[i].y() - centerP.y()) / dy);
		else
			m_drawPath[i] = QPointF(m_drawPath[i].x() + mx
				, m_drawPath[i].y() + my);
	}
}

void Shape::SvgFreeLine::drawShape(QPainter* painter, const double& m_scaling)
{
	painter->save();
	QPen pen;
	pen.setColor(m_borderColor);
	pen.setWidth(m_borderWidth);
	//pen.setStyle(m_penStyle);
	painter->setPen(pen);
	QVector<QPointF> path = m_drawPath;
	if (0 != m_ratotation)
	{
		for (int i = 0; i < path.size(); i++)
			path[i] = (path[i] - getCenterPointF()) * m_scaling;
		painter->translate(QPointF(getCenterPointF() * m_scaling));
		painter->rotate(m_ratotation);
	}
	else
	{
		for (int i = 0; i < path.size(); i++)
			path[i] = (path[i] * m_scaling);
	}
	int size = path.size();
	int i = 0;
	while (i < size)
	{
		QVector<QPointF> sub_path;
		if (0 != i)
		{
			sub_path.push_back(path[i - 1]);
		}
		for (int j = 0; i < size && j < 3; ++j)
		{
			sub_path.push_back(path[i++]);
		}
		painter->drawPolyline(sub_path);
	}
	//painter.drawPolyline(path);
	painter->restore();
}

bool Shape::SvgFreeLine::isContains(const QPointF& pos) const
{
	QPointF clickp = pos;
	if (0 != m_ratotation)
	{
		double x = (pos.x() - getCenterPointF().x()) * cos(-m_ratotation / 180 * PI)
			- (pos.y() - getCenterPointF().y()) * sin(-m_ratotation / 180 * PI)
			+ getCenterPointF().x();
		double y = (pos.x() - getCenterPointF().x()) * sin(-m_ratotation / 180 * PI)
			+ (pos.y() - getCenterPointF().y()) * cos(-m_ratotation / 180 * PI)
			+ getCenterPointF().y();
		clickp = QPointF(x, y);
	}
	if (0 == m_drawPath.size())
		return false;
	double length1
		= sqrt
		(pow(abs(m_drawPath[0].x() - clickp.x()), 2) +
			pow(abs(m_drawPath[0].y() - clickp.y()), 2));
	if (5 >= length1)
		return true;
	//假设将路径的首位相连形成一个多边形
	QPainterPath path;
	path.addPolygon(QPolygonF(m_drawPath));
	path.setFillRule(Qt::WindingFill);
	return path.contains(clickp);
}

