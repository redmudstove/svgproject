#ifndef SVGRECTANGLE_H_
#define SVGRECTANGLE_H_
#include "kernel/shape/svgabstractshape.h"
namespace Shape{
	class SvgRectangle :public Shape::SvgAbstractShape
	{
	public:
		SvgRectangle();
		~SvgRectangle();
		bool isSelect() const override;
		bool isContains(const QPointF& pos) const override;
		bool isContains(const double& x, const double& y)  const override;
		void zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP) override;
		void drawShape(QPainter* painter, const double& m_scaling) override;
	protected:
	private:
	};

}

#endif
