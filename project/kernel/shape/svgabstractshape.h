﻿#ifndef SVGABSTRACTSHAPE_H_
#define SVGABSTRACTSHAPE_H_
#include <QColor>
/// <summary>
/// 图形的抽象类
/// </summary>
/// 
/// 
/// m_selectRect
#include <QRectF>
#include <QVector>
const double PI = 3.1415926;
class QPainter;
namespace Shape {
	enum class BorderLineStyle {


	};
	/// <summary>
	/// 所有svg支持的图形类
	/// </summary>
	enum class ShapeType
	{
		select=0,
		line=1,
		rectangle=2,
		circle=3,
		ellipse=4,
		polyline=5,
		polygon=6,
		path=7,
		text=8,
		eyedroper=9,
		shapeZoom=10,
		freeLine = 11
	};
	/// <summary>
	/// class of abstract shape 
	/// </summary>
	class SvgAbstractShape
	{
	public:
		explicit SvgAbstractShape();
		virtual ~SvgAbstractShape();
		virtual bool isSelect() const = 0;
		virtual bool isContains(const QPointF& pos) const = 0;
		virtual bool isContains(const double& x, const double& y)const  = 0;
		virtual void drawShape(QPainter* painter, const double& m_scaling) = 0;
		int getRatotion()const;
		int getOpacity() const;
		int getBlur()const;
		int getBorderWidth()const;
		void setBorderColor(const QColor&);
		void setBackgroundColor(const QColor&);
		QColor getBorderColor()const;
		QColor getBackgroundColor()const;
		void setBorderStyle(const BorderLineStyle& borderstyle);
		void setRatotion(const int&);
		void setOpacity(const int& value);
		void setBlur(const int&);
		void setBorderWidth(const int&);
		void setPenStyle(const Qt::PenStyle style);
		//对图形缩放
		virtual void zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP) = 0;
		virtual void moveTo(const QPointF&);
		virtual void moveTo(const double& x, const double& y);
		ShapeType getShapeType()const;
		void setShapeType(const ShapeType&);
		void setStartPosition(const QPointF&);
		void setEndPosition(const QPointF&);
		QPointF getStartPosition()const;
		QPointF getEndPosition()const;
		void setDrawend(const bool flags);
		bool getDrawend()const;

		void setPath(const QVector<QPointF>&);
		QVector<QPointF> getPath() const;

		QRectF getRectF() const;
		virtual int getPositionX() const;
		virtual int getPositionY() const;
		virtual int getRectW() const ;
		virtual int getRectH() const;
		QPointF getCenterPointF() const;
	protected:
		ShapeType m_shapeType;
		QColor m_borderColor;
		QColor m_backgroundColor;
		int m_ratotation;
		//透明度
		int m_Opacity;
		//模糊
		int m_blur;
		int m_borderWidth;
		BorderLineStyle m_borderstyle;
		QPointF m_stPos;
		QPointF m_enPos;
		QVector<QPointF> m_drawPath;
	protected:
			bool isDrawend;
		
	};
}
#endif
