#ifndef SVGELLIPSE_H_
#define SVGELLIPSE_H_
#include "kernel/shape/svgabstractshape.h"
namespace Shape
{
	class SvgEllipse :public Shape::SvgAbstractShape
	{
	public:
		SvgEllipse();
		~SvgEllipse();
		bool isSelect() const override;
		bool isContains(const double& x, const double& y)const override;
		bool isContains(const QPointF& pos) const override;
		void drawShape(QPainter* painter, const double& m_scaling) override;
		void zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP) override;
	protected:
	private:
	};
}
#endif
