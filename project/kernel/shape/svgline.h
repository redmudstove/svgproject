#ifndef SVGLINE_H_
#define SVGLINE_H_
#include "kernel/shape/svgabstractshape.h"
#include <QPainter>

class QPainter;
extern const double PIE;
namespace Shape{
	class SvgLine :public SvgAbstractShape {
	public:
		SvgLine();
		~SvgLine();
		bool isSelect() const override;
		//bool isContains(const QPointF& pos) const override;
		//bool isContains(const double& x, const double& y) override;
		virtual bool isContains(const QPointF& pos) const override;
		virtual bool isContains(const double& x, const double& y) const override;
		void drawShape(QPainter* painter, const double& m_scaling) override;
		void moveTo(const QPointF&) override;
		void setPosition(const QPointF&, const QPointF&);
		void zoomShape(const double& scaleX, const double& scaleY, const QPointF& centerP) override;
		
	private:

	};
}


#endif
