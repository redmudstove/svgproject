#ifndef SHAPEEDIT_H_
#define SHAPEEDIT_H_
#include <QPlainTextEdit>
class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;
class ShapeEdit:public QPlainTextEdit
{
Q_OBJECT
public:
	ShapeEdit(QWidget* parent = nullptr);
	~ShapeEdit();
	void lineNumberAreaPaintEvent(QPaintEvent* event);
	int lineNumberAreaWidth();
protected:
	void resizeEvent(QResizeEvent* event) override;

private slots:

	void updateLineNumberAreaWidth(int newBlockCount);
	void highlightCurrentLine();
	void updateLineNumberArea(const QRect& rect, int dy);

private:
	QWidget* lineNumberArea;
};

class LineNumberArea:public QWidget
{
	Q_OBJECT
public:
	LineNumberArea(ShapeEdit* editor):QWidget(editor), codeEditor(editor){}
	QSize sizeHint() const override 
	{
		return QSize(codeEditor->lineNumberAreaWidth(), 0);
	}
protected:
	void paintEvent(QPaintEvent* event) override {
		codeEditor->lineNumberAreaPaintEvent(event);
	}
private:
	ShapeEdit* codeEditor;
};

#endif
