﻿#ifndef MAINAPPLICATION_H_
#define MAINAPPLICATION_H_
#include <QWidget>
#include <QRect>
//#include "components/controlwidget/settingwidget.h"
#include "components/topwidget/toptaskbarwidget.h"
#include "components/controlwidget/topfunctionbarwidget.h"
#include "components/topwidget/toppagewidget.h"
#include "components/workstationwidget/svgworkstationwidget.h"
#include <QVector>
static int countnuber = 0;
class MainApplication: public QWidget
{
	Q_OBJECT
public:
	MainApplication(QWidget* parent = nullptr);
	~MainApplication();
	void setAreaMovable(const QRect rt);
public slots:
	void changeStates(int states);
protected:
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void paintEvent(QPaintEvent* event) override;
private slots:
	void setChangedtoToppage();
	void deleteWorkStation(const int& index);
	void createNewWorkStation(const int& index);
	void setSelectWorkStation(const int& index);
private:
	void setWidgetProperties();
	void initQssFile();
private:
	QRect m_areaMovable;
	bool m_bPressed;
	QPoint m_ptPress;
	QVBoxLayout* m_vb = nullptr;
	int m_curIndex;
private:
	void initWidget();
	void initSignals();
private:
	TopTaskBarWidget* m_setting = nullptr;
	TopPageWidget* m_fuctionbar = nullptr;
	SvgWorkStationWidget* m_svgworkstationwidget = nullptr;
	QVector<QWidget*> m_worklist;
	
};


#endif
