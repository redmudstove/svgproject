#ifndef OPENSPLASH_H_
#define OPENSPLASH_H_
#include <QSplashScreen>
#include <qpropertyanimation.h>
class OpenSplash:public QSplashScreen
{
	Q_OBJECT
public:
	explicit OpenSplash(const QPixmap& pixmap);
	~OpenSplash();
	static OpenSplash* getInstance();
protected:
	void paintEvent(QPaintEvent* event) override;
public:
	void setStagePercent(const int&percent,const QString &message);
	void setStart(QWidget* widget, const QString& title, const QString& loadFile);
	void setFinish();
private:
	double m_percent;
	QWidget* m_mainWidget;
	QPixmap m_pixLogo;
	QString m_textLogo;
	QString m_message;
	static OpenSplash* m_instance;
	QPropertyAnimation* m_propertyAnimation;
private:
	const int ANIMATION_DURATION = 1000;


};



#endif
