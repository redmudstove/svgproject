﻿#include "components/startloadpage/opensplash.h"
#include <QApplication>
#include <QPixmap>
#include <QPainter>
#include <QFont>
#include <QPen>
#include <QBrush>
#include <QString>
#include <QPainterPath>
#include <QRectF>
OpenSplash* OpenSplash::m_instance = nullptr;
OpenSplash::OpenSplash(const QPixmap& pixmap):QSplashScreen(pixmap)
	,m_percent(0)
	,m_mainWidget(nullptr)
	,m_propertyAnimation(nullptr)
{

}

OpenSplash::~OpenSplash()
{

}

OpenSplash* OpenSplash::getInstance()
{
	if (nullptr == m_instance) {

		m_instance = new OpenSplash(QPixmap(":/startup/res/icons/ui/widget/startpage/startpage.jpg"));

	}
	return m_instance;
}

void OpenSplash::paintEvent(QPaintEvent* event)
{
	Q_UNUSED(event);
	const int OFFSET_VALUE = 70;
	const int SLIDER_HEIGHT = 10;
	const int BORDER_X_RADIUS = 8;
	const int BORDER_Y_RADIUS = 4;
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	QFont font(QString("微软雅黑"));
	QPen pen;
	pen.setColor(Qt::white);
	painter.setPen(pen);
	painter.drawRoundedRect(OFFSET_VALUE, this->height() - OFFSET_VALUE,
		(this->width() - OFFSET_VALUE*2),
		BORDER_X_RADIUS, BORDER_Y_RADIUS,SLIDER_HEIGHT);
	{

		font.setPixelSize(16);
		font.setBold(true);
		painter.setFont(font);
		pen.setColor(Qt::white);
		painter.setPen(pen);
		painter.drawText(0, this->height() - OFFSET_VALUE *3+5, this->width(), OFFSET_VALUE, Qt::AlignCenter, m_message);

	} 
	{
		pen.setColor(Qt::white);
		painter.setPen(pen);
		if (m_percent != 0)
		{
			//QBrush brush(QColor(10, 230, 100, 255));
			QBrush brush(QColor("#1296db"));
			painter.setBrush(brush);
			painter.drawRoundedRect(OFFSET_VALUE, this->height() - OFFSET_VALUE
				, (this->rect().width() - OFFSET_VALUE * 2) * m_percent / 100
				, BORDER_X_RADIUS, BORDER_Y_RADIUS, SLIDER_HEIGHT);
		}
		{

			font.setPixelSize(14);
			font.setBold(false);
			painter.setFont(font);
			pen.setColor(Qt::white);
			painter.setPen(pen);
			QString drawText = QString::number(m_percent, 'f', 0) + QString("%");
			painter.drawText(0, this->height() - OFFSET_VALUE, this->width(), OFFSET_VALUE, Qt::AlignCenter, drawText);
			painter.end();

		}
		{
			QPainterPath painterPathPath;
			QRectF rect = QRectF(0, 0, this->width(), this->height());
			painterPathPath.addRoundRect(rect, BORDER_X_RADIUS, BORDER_X_RADIUS);
			QPolygon polygon = painterPathPath.toFillPolygon().toPolygon();
			QRegion region(polygon);
			this->setMask(region);
		}
	
	}
}

void OpenSplash::setStagePercent(const int& percent, const QString& message)
{
	if (this->isHidden()) {
		this->show();
	}
	if (!this->isActiveWindow()) {
		this->activateWindow();
		this->raise();
	}
	m_message = message;
	while (m_percent<percent)
	{
		m_percent = m_percent + 0.02;
		qApp->processEvents();
		this->repaint();
	}
}

void OpenSplash::setStart(QWidget* widget, const QString& title, const QString& loadFile)
{
	if (nullptr != widget) {
		m_mainWidget = widget;
		m_pixLogo = QPixmap(loadFile);
		m_textLogo = title;
		m_mainWidget->setWindowOpacity(0.0);
		if (nullptr == m_propertyAnimation) {
			m_propertyAnimation = new QPropertyAnimation(m_mainWidget,"windowOpacity");
			m_propertyAnimation->setDuration(ANIMATION_DURATION);
			m_propertyAnimation->setStartValue(0.0);
			m_propertyAnimation->setEndValue(1.0);
		}
	}
}

void OpenSplash::setFinish()
{
	this->close();
	if (nullptr != m_mainWidget) {
		m_mainWidget->activateWindow();
		m_mainWidget->raise();
	}
	if (nullptr != m_propertyAnimation) {
		m_propertyAnimation->start();
	}
}
