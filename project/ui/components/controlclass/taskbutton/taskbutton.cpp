﻿#include "components/controlclass/taskbutton/taskbutton.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
#include <QTimer>
TaskButton::TaskButton(QString fileName, int index, QWidget* parent /*= nullptr*/) :QPushButton(parent), isPressed(false), m_name(fileName),m_index(index)
{
	//this->setFixedSize(100,35);
	initWidget();
	initSignals();
	//QTimer* timer = new QTimer(this); //this 为parent类, 表示当前窗口
	//connect(timer, &QTimer::timeout, this, &TaskButton::sendchecked); // SLOT填入一个槽函数
	//timer->start(500);

}

TaskButton::~TaskButton()
{

}

void TaskButton::setName(const QString name)
{
	m_name = name;
}

QString TaskButton::getName() const
{
	return this->m_name;
}

int TaskButton::getIndex() const
{
	return m_index;
}

void TaskButton::setIsPressed(const bool& flags)
{
	isPressed = flags;
}

void TaskButton::deleteBtn()
{
	emit sendDeletesignlas(m_index);
	emit sendDeleteBtn();

}

void TaskButton::sendchecked()
{
	if (this->isChecked()) {
		emit sendSelected(true,m_index);
	}
}

void TaskButton::setButtonChecked(bool flags)
{
	//如果没有按下的时候，触发按钮的按下信号则表示需要按下，设置为已经按下。
	if (!isPressed) {
		isPressed = true;
		this->setChecked(true);
		emit sendIspressedIndex(m_index);
	}
}

void TaskButton::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	
	QPushButton::paintEvent(event);
}

//void TaskButton::mousePressEvent(QMouseEvent* event)
//{
//	
//	QPushButton::mousePressEvent(event);
//}

void TaskButton::enterEvent(QEvent* event)
{
	this->setCursor(Qt::PointingHandCursor);
	QPushButton::enterEvent(event);
}

void TaskButton::initSignals()
{
	connect(m_deleteBtn, &QPushButton::clicked, this,&TaskButton::deleteBtn);
}

//void TaskButton::mouseReleaseEvent(QMouseEvent* event)
//{
//
//}

void TaskButton::initButton()
{

}

void TaskButton::initWidget()
{
	m_hb = new QHBoxLayout;
	m_hb->addSpacing(5);
	m_icon = new QLabel(this);
	m_icon->setFixedSize(16, 16);
	m_icon->setObjectName("taskbuttonicon");
	m_fileName = new QLabel(this);
	m_fileName->setFixedSize(60, 16);
	m_fileName->setText(m_name);
	m_deleteBtn = new QPushButton(this);
	m_deleteBtn->setFixedSize(20, 20);
	m_deleteBtn->setObjectName("taskbuttondelete");
	m_hb->addWidget(m_icon);
	m_hb->addWidget(m_fileName);
	m_hb->addStretch();
	m_hb->addWidget(m_deleteBtn);
	m_hb->setContentsMargins(0, 0, 0, 0);
	this->setLayout(m_hb);
}

