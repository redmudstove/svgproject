﻿//用于自定义任务按钮的类
#ifndef TASKBUTTON_H_
#define TASKBUTTON_H_
#include  <QPushButton>;
#include <QColor>
class QLabel;
class QHBoxLayout;
class TaskButton: public QPushButton
{
	Q_OBJECT
public:
	TaskButton(QString fileName,int index,QWidget* parent = nullptr);
	~TaskButton();
public:
	void setName(const QString name);
	QString getName() const;
	int getIndex() const;
	void setIsPressed(const bool& flags);

public slots:
	void setButtonChecked(bool flags);
private slots:
	void deleteBtn();
	void sendchecked();
	
signals:
	void sendDeleteBtn();
	void sendDeletesignlas(const int&index);
	void sendSelected(bool flags,const int&index);
	void sendIspressedIndex(int index);
protected:
	void paintEvent(QPaintEvent* event) override;
	//void mousePressEvent(QMouseEvent* event) override;
	//void mouseReleaseEvent(QMouseEvent* event) override;
	void enterEvent(QEvent* event) override;
private:
	void initSignals();
	void initButton();
	void initWidget();
private:
	QPushButton* m_deleteBtn = nullptr;
	QLabel* m_fileName = nullptr;
	QLabel* m_icon = nullptr;
	QHBoxLayout* m_hb = nullptr;

private:
	bool isPressed;
	QColor m_color;
	QString m_name;
	int m_index;
};




#endif