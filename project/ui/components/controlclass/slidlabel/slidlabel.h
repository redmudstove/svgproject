#ifndef SLIDLABEL_H_
#define SLIDLABEL_H_
#include <QLabel>
class SlidLabel:public QLabel
{
	Q_OBJECT
public:
	SlidLabel(QWidget *parent=nullptr);
	~SlidLabel();
public:
signals:
	void wheelslip(const bool flags);
protected:
	void paintEvent(QPaintEvent* event) override;
	void enterEvent(QEvent* event) override;
	void leaveEvent(QEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* ev) override;
private:
	double m_startY;
	double m_endY;
};



#endif
