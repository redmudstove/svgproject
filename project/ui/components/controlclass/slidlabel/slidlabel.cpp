#include "ui/components/controlclass/slidlabel/slidlabel.h"
#include <QEvent>
#include <QPaintEvent>
#include <QStyleOption>
#include <QPainter>
#include <QMouseEvent>

SlidLabel::SlidLabel(QWidget* parent/*=nullptr*/):
	QLabel(parent)
{

}

SlidLabel::~SlidLabel()
{

}
void SlidLabel::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QLabel::paintEvent(event);
}

void SlidLabel::enterEvent(QEvent* event)
{
	this->setCursor(QCursor(QPixmap(":/cursor/res/icons/ui/control/mousercurosr/sizered.png"),-1,-1));
}

void SlidLabel::leaveEvent(QEvent* event)
{
	this->setCursor(Qt::ArrowCursor);
}

void SlidLabel::mousePressEvent(QMouseEvent* event)
{
	m_startY = event->localPos().y();
	m_endY = event->localPos().x();
}

void SlidLabel::mouseMoveEvent(QMouseEvent* event)
{
	m_startY = m_endY;
	m_endY = event->localPos().y();
	this->setCursor(QCursor(QPixmap(":/cursor/res/icons/ui/control/mousercurosr/sizered.png"), -1, -1));
	if (m_endY < m_startY)
		emit wheelslip(true);
	else if (m_endY>m_startY)
	{
		emit wheelslip(false);
	}
}

void SlidLabel::mouseReleaseEvent(QMouseEvent* ev)
{

}

