#include "components/controlclass/svgpropertywidget/svgpropertywidget.h"
#include "components/controlclass/graphicsprocess/shapefunctionwidget/shapefunctionwidget.h"
#include "components/controlclass/graphicsprocess/shapealignwidget/shapealignwidget.h"
#include "components/controlclass/graphicsprocess/shapeappearancewidget/shapeappearancewidget.h"
#include "components/controlclass/graphicsprocess/shapeparameterwidget/shapeparameterwidget.h"
#include "components/controlclass/graphicsprocess/shapeshadewidget/shapeshadewidget.h"
#include "components/controlclass/graphicsprocess/shapestrokewidget/shapestrokewidget.h"
#include "components/controlclass/graphicsprocess/shapefillwidget/shapefillwidget.h"
#include "components/controlclass/graphicsprocess/shapeobscurewidget/shapeobscurewidget.h"
#include "components/controlclass/graphicsprocess/svgcanvaspropertywidget/svgcanvaspropertywidget.h"

#include <QVBoxLayout>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
SvgPropertyWidget::SvgPropertyWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

SvgPropertyWidget::~SvgPropertyWidget()
{

}

void SvgPropertyWidget::setPolygonShapeParmater(const int& w, const int& h, const int& x, const int& y, const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor)
{
	
	emit sendParameter(w, h, x, y, ration);
	emit sendShapesTrans(trans);
	emit sendShapesFillcorandTrans(fillcolor,trans);
}

void SvgPropertyWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void SvgPropertyWidget::initWidget()
{
	m_vb = new QVBoxLayout;
	m_shapePwidget = new ShapeParameterWidget(this);
	m_shapeAwidget = new ShapeAlignWidget(this);
	m_shapeAprewidget = new ShapeAppearanceWidget(this);
	m_shapeFwidget = new ShapeFillWidget(this);
	m_shapeStwidget = new ShapeStrokeWidget(this);
	m_shapeObwidget = new ShapeObscureWidget(this);
	
	m_vb->addWidget(m_shapeAwidget);
	m_vb->addWidget(m_shapePwidget);
	m_vb->addWidget(m_shapeAprewidget);
	m_vb->addWidget(m_shapeFwidget);
	m_vb->addWidget(m_shapeStwidget);
	m_vb->addWidget(m_shapeObwidget);
	m_vb->setContentsMargins(0, 0, 0, 0);
	m_vb->setSpacing(1);
	this->setLayout(m_vb);

}

void SvgPropertyWidget::initSignals()
{
	connect(this, &SvgPropertyWidget::sendParameter, m_shapePwidget, &ShapeParameterWidget::setParameter);
	connect(this, &SvgPropertyWidget::sendShapesTrans, m_shapeAprewidget, &ShapeAppearanceWidget::setShapeTrans);
	connect(this, &SvgPropertyWidget::sendShapesFillcorandTrans, m_shapeFwidget, &ShapeFillWidget::setShapefillParameter);

	connect(m_shapeAprewidget, &ShapeAppearanceWidget::sendSlidervalue, this, [=](const qreal& value) {
		emit sendSliderTransvalue(value);
		});
	connect(m_shapeFwidget, &ShapeFillWidget::sendSelectColor, [=](const QColor&color) {
		emit sendSelectColor(color);
		});
	connect(m_shapeStwidget, &ShapeStrokeWidget::sendSelectStrokeColor, this, [=](const QColor&color) {
		emit sendSelectStrokeColor(color);
		});
	
}

