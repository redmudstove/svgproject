﻿#ifndef SVGPROPERTYWIDGET_H_
#define SVGPROPERTYWIDGET_H_
#include <QWidget>
//class ShapeFunctionWidget;
class ShapeParameterWidget;
class ShapeAppearanceWidget;
class ShapeAlignWidget;
class ShapeFillWidget;
class ShapeObscureWidget;
class ShapeStrokeWidget;
class QVBoxLayout;
class QStackedWidget;
class SvgCanvasProWidget;

/// <summary>
/// svg的属性栏
/// </summary>
class SvgPropertyWidget:public QWidget
{
	Q_OBJECT
public:
	SvgPropertyWidget(QWidget* parent = nullptr);
	~SvgPropertyWidget();

public slots:
	void setPolygonShapeParmater(const int& w, const int& h, const int& x,
		const int& y, const int& ration, const int& trans, 
		const int& blur, const QColor& fillcolor, const QColor& bordercolor);
signals:
	void sendParameter(const int&,const int&,const int &,const int&,const int&);
	void sendSliderTransvalue(const int& value);
	void sendShapesTrans(const int& value);
	void sendSelectColor(const QColor& color);
	void sendShapesFillcorandTrans(const QColor& color, const int& trans);
	void sendSelectStrokeColor(const QColor& color);
protected:
	void paintEvent(QPaintEvent* event) override;


private:
	void initWidget();
	void initSignals();
private:
	//ShapeFunctionWidget* m_shapeFucwidgte = nullptr;
	ShapeParameterWidget* m_shapePwidget = nullptr;
	ShapeAlignWidget* m_shapeAwidget = nullptr;
	ShapeFillWidget* m_shapeFwidget = nullptr;
	ShapeObscureWidget* m_shapeObwidget = nullptr;
	ShapeStrokeWidget* m_shapeStwidget = nullptr;
	ShapeAppearanceWidget* m_shapeAprewidget = nullptr;
	QVBoxLayout* m_vb = nullptr;
};



#endif