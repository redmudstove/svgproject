﻿#ifndef COLORBARWIDGET_H_
#define COLORBARWIDGET_H_
#include <QWidget>
#include "components/controlclass/colorbutton/colorbutton.h"
const QVector<QString> colorType = {
"#d81e06","#f4ea29","#19fa28","#1196db","#13227a","#d42379","#ffffff","#e6e6e6","#dbdbdb","#cdcdcd","#bfbfbf","#8a8a8a","#707070","#515151","#2c2c2c","#000000",
"#ea986c","#eeb173","#f3ca7e","#f9f28b","#c8db8c","#aad08f","#87c38f","#83c6c2","#7dc5eb","#87a7d6","#8992c8","#a686ba","#bd8cbb","#be8dbd","#e89abe","#e8989a",
"#e16531","#e98f36","#efb336","#f6ef37","#afcd50","#7cba59","#36ab60","#1aaba8","#17abe3","#3f81c1","#4f68b0","#594d9c","#82529d","#a4579d","#db639b","#dd6572",
"#d81e06","#e0620d","#ea9518","#f4ea29","#8cbb19","#2aa515","#0e932e","#0b988f","#1195db","#0061b2","#0061b0","#004198","#112079","#88147f","#d3217b","#d6204b"
};
const QVector<QColor> colorRgbType ={ 
QColor(216,30,6),QColor(244,234,42), QColor(26,250,41), QColor(18,150,219), QColor(19,34,122), QColor(212,35,122), QColor(255,255,255), QColor(230,230,230), QColor(219,219,219), QColor(205,205,205), QColor(191,191,191), QColor(138,138,138), QColor(112,112,112), QColor(81,81,81), QColor(44,44,44), QColor(0,0,0),
QColor(234,152,108), QColor(238,177,116), QColor(243,202,126), QColor(249,242,139), QColor(200,219,140), QColor(170,208,143), QColor(135,195,143), QColor(131,198,194), QColor(125,197,235), QColor(135,167,214), QColor(137,146,200), QColor(166,134,186), QColor(189,140,187), QColor(190,141,189), QColor(232,154,190), QColor(232,152,154),
QColor(225,102,50), QColor(233,143,54), QColor(239,179,54), QColor(246,239,55), QColor(175,205,81), QColor(124,186,89), QColor(54,171,96), QColor(27,171,168), QColor(23,172,227), QColor(63,129,193), QColor(79,104,176), QColor(89,77,156), QColor(130,82,157), QColor(164,87,157), QColor(219,100,155), QColor(221,101,114),
QColor(216,30,6), QColor(224,98,13), QColor(234,149,24), QColor(244,234,42), QColor(140,187,26), QColor(43,165,21), QColor(14,147,46), QColor(12,152,144), QColor(18,149,219), QColor(0,97,178), QColor(0,97,176), QColor(0,65,152), QColor(18,33,121), QColor(136,20,127), QColor(211,34,123), QColor(214,32,75)
};

class QButtonGroup;
class QGridLayout;
/// <summary>
/// 这个类是用来显示颜色按钮
/// </summary>
class ColorBarWidget:public QWidget
{
	Q_OBJECT
public:
	ColorBarWidget(QWidget* parent = nullptr);
	~ColorBarWidget();

signals:
	void sendSelectedColor(const QColor&color);
protected:
	void paintEvent(QPaintEvent* event) override;
	//void enterEvent(QEvent* event) override;
private:
	void initWidget();
	void initSignals();
private:
	QGridLayout* m_grid = nullptr;
	QButtonGroup* m_group = nullptr;

};


#endif // !COLORBARWIDGET_H_
