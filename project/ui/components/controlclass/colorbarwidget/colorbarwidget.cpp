#include "components/controlclass/colorbarwidget/colorbarwidget.h"
#include <QGridLayout>
#include <QStyleOPtion>
#include <QPainter>
#include <QButtonGroup>
#include <qdialog.h>
#include <QDebug>
ColorBarWidget::ColorBarWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

ColorBarWidget::~ColorBarWidget()
{

}

void ColorBarWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void ColorBarWidget::initWidget()
{
	m_group = new QButtonGroup;
	m_grid = new QGridLayout;
	int indexofColor = 0;
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 16; ++j) {
			ColorButton* btn = new ColorButton(QColor(colorType[indexofColor]), this);
			btn->setFixedSize(16, 16);
			m_grid->addWidget(btn, i, j);
			m_group->addButton(btn, indexofColor);
			indexofColor++;
		}
	}
	m_grid->setHorizontalSpacing(1);
	m_grid->setVerticalSpacing(1);
	m_grid->setContentsMargins(1, 1, 1, 1);
	this->setLayout(m_grid);
}

void ColorBarWidget::initSignals()
{
	connect(m_group, &QButtonGroup::idClicked, this, [=](const int&id) {
		emit sendSelectedColor(QColor(colorType[id]));
		});
}

