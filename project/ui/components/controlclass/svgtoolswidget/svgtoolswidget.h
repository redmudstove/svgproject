﻿#ifndef SVGTOOLSWIDGET_H_
#define SVGTOOLSWIDGET_H_
#include <QWidget>
class QVBoxLayout;
class QStackedWidget;
class SvgPropertyWidget;
class ShapeFunctionWidget;
class SvgCanvasProWidget;
class SvgToolsWidget:public QWidget
{
	Q_OBJECT
public:
	SvgToolsWidget(QWidget* parent = nullptr);
	~SvgToolsWidget();

public slots:
	void setSelectedCanvas();
	void setSelectedShapes(const int& index, const int& w, const int& h, const int& x, const int& y,
		const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor);
protected:
	void paintEvent(QPaintEvent* event) override;
signals:
	void sendCanvasSize(const int w, const int h);
	void setPolygonShapeParmater(const int& w, const int& h, const int& x, const int& y, const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor);
	//更改透明度
	void sendTransvalue(const int& value);
	void sendSelectColor(const QColor& color);
	void sendSelectStrokeColor(const QColor& color);
private:
	void initWidget();
	void initSignals();
private:
	QVBoxLayout* m_vb = nullptr;
	QStackedWidget* m_stacked = nullptr;
	ShapeFunctionWidget* m_topfunction = nullptr;
	/// <summary>
	/// 这一个页面多边形共用。
	/// </summary>
	SvgPropertyWidget* m_svgpropertywidget = nullptr;
	SvgPropertyWidget* m_svgtestpro1 = nullptr;
	SvgCanvasProWidget* m_svgcanvasProperty = nullptr;


};
#endif
