﻿#include "components/controlclass/svgtoolswidget/svgtoolswidget.h"
#include "components/controlclass/svgpropertywidget/svgpropertywidget.h"
#include "components/controlclass/graphicsprocess/shapefunctionwidget/shapefunctionwidget.h"
#include "components/controlclass/graphicsprocess/svgcanvaspropertywidget/svgcanvaspropertywidget.h"

#include <QVBoxLayout>
#include <QStackedWidget>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
SvgToolsWidget::SvgToolsWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

SvgToolsWidget::~SvgToolsWidget()
{

}

void SvgToolsWidget::setSelectedCanvas()
{
	m_stacked->setCurrentIndex(0);
}

void SvgToolsWidget::setSelectedShapes(const int& index, const int& w, const int& h, const int& x, const int& y, const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor)
{
	
	/*m_stacked->setCurrentIndex(1);
	emit */
	switch (index)
	{
		//直线
	case 1:
	{

	}
		break;
		//自由线段
	case 2:
	{
		m_stacked->setCurrentIndex(2);
		emit setPolygonShapeParmater(w, h, x, y, ration, trans, blur, fillcolor, bordercolor);
	}
		break;

		//其他的集合
		
	case 3:
	{
	
	}
		break;
	default:
		break;
	}
}

void SvgToolsWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void SvgToolsWidget::initWidget()
{
	m_vb = new QVBoxLayout;
	m_topfunction = new ShapeFunctionWidget(this);
	m_svgpropertywidget = new SvgPropertyWidget(this);
//____________________________________________
	m_svgtestpro1 = new SvgPropertyWidget(this);
//_____________________________________________
	m_svgcanvasProperty = new SvgCanvasProWidget(this);
	m_stacked = new QStackedWidget(this);
	
	m_stacked->addWidget(m_svgcanvasProperty);
	m_stacked->addWidget(m_svgtestpro1);//1
	m_stacked->addWidget(m_svgpropertywidget);//2
	m_stacked->setCurrentIndex(0);
	m_vb->addWidget(m_topfunction);
	m_vb->addWidget(m_stacked);
	m_vb->setContentsMargins(0, 0, 0, 0);
	m_vb->setSpacing(1);
	this->setLayout(m_vb);
}

void SvgToolsWidget::initSignals()
{
	connect(m_svgcanvasProperty, &SvgCanvasProWidget::sendCanvasSize, this, [=](const int w, const int h) {
		emit sendCanvasSize(w, h);
		});
	connect(this, &SvgToolsWidget::setPolygonShapeParmater, m_svgpropertywidget, &SvgPropertyWidget::setPolygonShapeParmater);
	connect(m_svgpropertywidget, &SvgPropertyWidget::sendSliderTransvalue, this, [=](const qreal&value) {
		emit sendTransvalue(value);
		});
	connect(m_svgpropertywidget, &SvgPropertyWidget::sendSelectColor, this, [=](const QColor&color) {
		emit sendSelectColor(color);
		});
	connect(m_svgpropertywidget, &SvgPropertyWidget::sendSelectStrokeColor, this, [=](const QColor& color) {
		emit sendSelectStrokeColor(color);
		});
}
