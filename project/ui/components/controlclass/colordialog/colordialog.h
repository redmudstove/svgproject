﻿#ifndef COLORDIALOG_H_
#define COLORDIALOG_H_
#include "components/controlclass/colorbarwidget/colorbarwidget.h"
#include "components/controlclass/colorslidbarwidget/hcolorsliderwidget.h"
#include "components/controlclass/colorslidbarwidget/colorpreviewwidget.h"
#include "components/controlclass/colorslidbarwidget/colorslidshowwidget.h"
#include <QWidget>
class QPushButton;
class QVBoxLayout;
class QHBoxLayout;
class QLineEdit;
/// <summary>
/// 颜色对话框
/// </summary>
class ColorDialog:public QWidget	
{
	Q_OBJECT
public:
	ColorDialog(QWidget* parent = nullptr);
	~ColorDialog();
	//设置当前的颜色
	void setCurColor(const QColor&);
	//得到当前的颜色
	QColor getColor() const;
	//得到rgb形式的颜色
	QRgb getRgb() const;
protected:
	void paintEvent(QPaintEvent* event) override;
	//void focusOutEvent(QFocusEvent* event) override/*;*/
signals:
	void sendDialogColorSelected(const QColor& cor);
	void sendSelectColor(const QColor& cor);
private:
	void initWidget();
	void initSignals();

private slots:
	void colorItemSelcSlot(const QColor& c);
	void colorEditfinished();
	void updateColorData(const int&h,const int&s,const int&v);
	void textchangedslot(QString strColor);
private:
	QColor m_color;

	QHBoxLayout* m_hb4 = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QHBoxLayout* m_hb3 = nullptr;
	QLineEdit* m_colorle = nullptr;

	//QPushButton* m_ColorDialogOkBtn = nullptr;
	
	ColorBarWidget* m_colorbar = nullptr;
	ColorPrewViewWidget* m_colorprivew = nullptr;
	ColorSlidShowWidget* m_colorslid = nullptr;
	HColorSliderWidget* m_hcolorslid = nullptr;
};




#endif
