﻿#include "components/controlclass/colordialog/colordialog.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStyleOption>
#include <QPainter>
#include <QPaintEvent>
#include <QLineEdit>
#include <QValidator>
#include <QRegExp>
#include <QColor>
#include <QDebug>
#pragma execution_character_set("utf-8")
ColorDialog::ColorDialog(QWidget* parent /*= nullptr*/):QWidget(parent)
	,m_color(QColor("#a597be"))
{
	initWidget();
	initSignals();
}

ColorDialog::~ColorDialog()
{

}

QRgb ColorDialog::getRgb() const
{
	return QRgb();
}

void ColorDialog::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	QPainter p(this);
	opt.init(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}




void ColorDialog::initWidget()
{
	this->setFocusPolicy(Qt::ClickFocus);
	m_vb = new QVBoxLayout;
	m_colorbar = new ColorBarWidget(this);
	m_colorslid = new ColorSlidShowWidget(this);
	m_colorslid->setFixedSize(256, 256);
	m_hcolorslid = new HColorSliderWidget(this);
	m_hcolorslid->setFixedSize(34, 270);
	m_colorle = new QLineEdit(this);
	m_colorle->setAlignment(Qt::AlignLeft);
	m_colorle->setFixedSize(120, 30);
	
	m_colorprivew = new ColorPrewViewWidget(this);
	m_colorprivew->setFixedSize(170, 30);


	m_hb1 = new QHBoxLayout;
	m_hb1->addWidget(m_colorbar);
	m_hb2 = new QHBoxLayout;
	m_hb2->addWidget(m_colorslid);
	m_hb2->addWidget(m_hcolorslid);
	m_hb3 = new QHBoxLayout;
	m_hb3->addWidget(m_colorprivew);
	m_hb3->addStretch();
	m_hb3->addWidget(m_colorle);

	m_colorle->setObjectName("colorDiaLineEdit");
	QRegExp rx("#(\\d?[a-f]?){0,6}");
	m_colorle->setValidator(new QRegExpValidator(rx, this));
	m_colorle->setText(tr("#a597be"));

	m_hb3->setContentsMargins(0, 0, 0, 0);
	m_vb->addLayout(m_hb1);
	m_vb->addLayout(m_hb2);
	m_vb->addLayout(m_hb3);
	m_vb->addStretch();
	m_vb->setSpacing(2);
	m_vb->setContentsMargins(5, 5, 5, 5);
	this->setLayout(m_vb);


}

void ColorDialog::initSignals()
{
	connect(m_hcolorslid, &HColorSliderWidget::hueChangedSignal, m_colorslid, &ColorSlidShowWidget::hueChangedSlot);
	connect(m_colorslid, &ColorSlidShowWidget::svChangedSignal, m_colorprivew, &ColorPrewViewWidget::svChangedSlot);
	connect(m_colorbar, &ColorBarWidget::sendSelectedColor, this,&ColorDialog::colorItemSelcSlot);
	connect(m_colorle, &QLineEdit::editingFinished, this, &ColorDialog::colorEditfinished);
	connect(m_colorprivew, &ColorPrewViewWidget::svChangedSignal, this, &ColorDialog::updateColorData);
	connect(m_colorle, &QLineEdit::textEdited, this, &ColorDialog::textchangedslot);
}

void ColorDialog::colorItemSelcSlot(const QColor& c)
{
	m_hcolorslid->setHue(c.hue());
	m_colorslid->setColor(c);
}

void ColorDialog::colorEditfinished()
{
	QString colorStr = m_colorle->text();
	switch (colorStr.length())
	{
	case 0:
	case 1:
	{
		//此时输入的为#
		m_colorle->setText("#000000");
	}
	break;
	case 2:
	{
		m_colorle->setText(colorStr.insert(1, "00000"));
	}
	break;
	case 3:
	{
		m_colorle->setText(colorStr.insert(1, "0000"));
	}
	break;
	case 4:
	{
		m_colorle->setText(colorStr.insert(1, "000"));
	}
	break;
	case 5:
	{
		m_colorle->setText(colorStr.insert(1, "00"));
	}
	break;
	case 6:
	{
		m_colorle->setText(colorStr.insert(1, "0"));
	}
	break;
	case 7:
	{
		m_colorle->setText(colorStr);
	}
	break;
	default:
		break;
	}
}

void ColorDialog::updateColorData(const int& h, const int& s, const int& v)
{
	QColor color;
	color.setHsv(h, s, v);
	QString strR = QString::number(color.red(), 16);
	QString strG = QString::number(color.green(), 16);
	QString strB = QString::number(color.blue(), 16);
	QString strRgb = QString("%1%2%3").arg(QString("%1").arg(strR.size() == 1 ? strR.prepend("0") : strR),
		QString("%1").arg(strG.size() == 1 ? strG.prepend("0") : strG),
		QString("%1").arg(strB.size() == 1 ? strB.prepend("0") : strB));
	m_colorle->setText("#"+strRgb);
	emit sendSelectColor(QColor("#"+strRgb));
}

void ColorDialog::textchangedslot(QString str)
{
	QString strColor = str.remove(0,1);
	int r, g, b;
	switch (strColor.length())
	{
	case 0:
	{
		r = g = b = 0;
	}
	break;
	case 1:
	case 2:
	{
		r = g = 0;
		bool ok;
		b = strColor.toInt(&ok, 16);
	}
	break;
	case 3:
	{
		QString strR = QString("%1%2").arg(strColor.left(1), strColor.left(1));
		QString strG = QString("%1%2").arg(strColor.mid(1, 1), strColor.mid(1, 1));
		QString strB = QString("%1%2").arg(strColor.right(1), strColor.right(1));
		bool ok;
		r = strR.toInt(&ok, 16);
		g = strG.toInt(&ok, 16);
		b = strB.toInt(&ok, 16);
	}
	break;
	case 4:
	{
		r = 0;
		QString strG = QString("%1").arg(strColor.left(2));
		QString strB = QString("%1").arg(strColor.right(2));
		bool ok;
		g = strG.toInt(&ok, 16);
		b = strB.toInt(&ok, 16);
	}
	break;
	case 5:
	{
		QString strR = QString("%1").arg(strColor.left(1));
		QString strG = QString("%1").arg(strColor.mid(1, 2));
		QString strB = QString("%1").arg(strColor.right(2));
		bool ok;
		r = strR.toInt(&ok, 16);
		g = strG.toInt(&ok, 16);
		b = strB.toInt(&ok, 16);
	}
	break;
	case 6:
	{
		QString strR = QString("%1").arg(strColor.left(2));
		QString strG = QString("%1").arg(strColor.mid(2, 2));
		QString strB = QString("%1").arg(strColor.right(2));
		bool ok;
		r = strR.toInt(&ok, 16);
		g = strG.toInt(&ok, 16);
		b = strB.toInt(&ok, 16);
	}
	break;
	default:
		break;
	}

	QColor color;
	color.setRgb(r, g, b);
	int h, s, v;
	h = color.hue();
	s  = color.saturation();
	v  = color.value();
	m_hcolorslid->setHue(h);
	m_colorslid->setHsv(h, s, v);
	m_colorprivew->setNewColor(color);
	emit sendSelectColor(color);
}

