﻿#ifndef SHAPEFUNCTIONWIDGET_H_
#define SHAPEFUNCTIONWIDGET_H_
#include <QWidget>
class QPushButton;
class QHBoxLayout;
class QButtonGroup;
/// <summary>
/// 画布的工具栏
/// </summary>
class ShapeFunctionWidget:public QWidget
{
	Q_OBJECT
public:
	ShapeFunctionWidget(QWidget* parent = nullptr);
	~ShapeFunctionWidget();
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initSignals();
private:
	QPushButton* m_shapedesignBtn = nullptr;
	QPushButton* m_antetypeBtn = nullptr;
	QPushButton* m_remarkBtn = nullptr;
	QHBoxLayout* m_hb = nullptr;
	QButtonGroup* m_group = nullptr;
};


#endif