﻿#include "components/controlclass/graphicsprocess/shapefunctionwidget/shapefunctionwidget.h"
#include <QHBoxLayout>
#include <QPushButton>
#include <QStyleOption>
#include <QPainter>
#include <QButtonGroup>
#pragma execution_character_set("utf-8")
ShapeFunctionWidget::ShapeFunctionWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

ShapeFunctionWidget::~ShapeFunctionWidget()
{

}

void ShapeFunctionWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void ShapeFunctionWidget::initWidget()
{
	m_group = new QButtonGroup;
	m_hb = new QHBoxLayout;
	m_shapedesignBtn = new QPushButton(this);
	m_shapedesignBtn->setText(tr("设计"));
	m_shapedesignBtn->setFixedSize(60, 45);
	m_antetypeBtn = new QPushButton(this);
	m_antetypeBtn->setText(tr("原型"));
	m_antetypeBtn->setFixedSize(60, 45);

	m_remarkBtn = new QPushButton(this);
	m_remarkBtn->setText(tr("标记"));
	m_remarkBtn->setFixedSize(60, 45);
	
	m_shapedesignBtn->setProperty("shapedesignBtn", "true");
	m_shapedesignBtn->setCheckable(true);
	m_group->addButton(m_shapedesignBtn, 0);
	m_shapedesignBtn->setChecked(true);

	m_antetypeBtn->setProperty("shapedesignBtn", "true");
	m_group->addButton(m_antetypeBtn,1);
	m_antetypeBtn->setCheckable(true);
	m_remarkBtn->setProperty("shapedesignBtn", "true");
	m_group->addButton(m_remarkBtn, 2);
	m_remarkBtn->setCheckable(true);
	m_group->setExclusive(true);

	m_hb->addWidget(m_shapedesignBtn);
	m_hb->addWidget(m_antetypeBtn);
	m_hb->addWidget(m_remarkBtn);
	m_hb->addStretch();
	m_hb->setSpacing(0);
	m_hb->setContentsMargins(0, 0, 0, 0);

	

	this->setLayout(m_hb);

}

void ShapeFunctionWidget::initSignals()
{

}

