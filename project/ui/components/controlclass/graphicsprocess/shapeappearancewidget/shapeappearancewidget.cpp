﻿#include "components/controlclass/graphicsprocess/shapeappearancewidget/shapeappearancewidget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QSlider>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
#pragma execution_character_set("utf-8")
ShapeAppearanceWidget::ShapeAppearanceWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

ShapeAppearanceWidget::~ShapeAppearanceWidget()
{

}

void ShapeAppearanceWidget::setShapeTrans(const int& value)
{
	m_slider->setValue(value);
}

void ShapeAppearanceWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void ShapeAppearanceWidget::initWidget()
{
	m_hb1 = new QHBoxLayout;
	m_functionname = new QLabel(this);
	m_functionname->setFixedSize(40, 30);
	m_functionname->setText(tr("外观"));
	m_functionname->setProperty("shapeprarameterlab", "true");
	m_comboBox = new QComboBox(this);
	m_comboBox->setFixedSize(85, 30);
	m_comboBox->addItem(tr("正常"));
	m_comboBox->addItem(tr("变暗"));
	m_comboBox->addItem(tr("正片叠加"));
	m_comboBox->addItem(tr("颜色加深"));
	m_comboBox->setCurrentIndex(0);

	m_hb1->addWidget(m_functionname);
	m_hb1->addWidget(m_comboBox);
	m_hb1->addStretch();
	m_hb1->setContentsMargins(16, 0, 0, 0);
	m_hb1->setSpacing(0);



	m_hb2 = new QHBoxLayout;
	m_opreationName = new QLabel(this);
	m_opreationName->setFixedSize(60,30);
	m_opreationName->setText(tr("不透明度"));
	m_slider = new QSlider(Qt::Horizontal,this);
	m_slider->setMaximum(255);
	m_slider->setMinimum(0);
	m_slider->setSingleStep(2);
	m_slider->setPageStep(2);
	m_slider->setFixedSize(120, 30);
	m_lineEdit = new QLineEdit(this);
	m_lineEdit->setFixedSize(64, 30);
	m_lineEdit->setAlignment(Qt::AlignRight);
	m_lineEdit->setText(tr("00")+" %");
	m_opreationName->setProperty("shapeprarameterlab", "true");
	m_lineEdit->setProperty("shapeedit", "true");
	m_hb2->addWidget(m_opreationName);
	m_hb2->addWidget(m_slider);
	m_hb2->addWidget(m_lineEdit);
	m_hb2->addStretch();
	m_hb2->setContentsMargins(16, 0, 0, 0);
	m_vb = new QVBoxLayout;
	m_vb->addLayout(m_hb1);
	m_vb->addLayout(m_hb2);
	m_vb->setContentsMargins(2, 2, 0, 2);

	
	this->setLayout(m_vb);



}

void ShapeAppearanceWidget::initSignals()
{
	connect(m_slider, &QSlider::valueChanged, this, [=](const int&value) {
		m_lineEdit->setText(QString::number((value / 255.00) * 100, 'd', 0)+" %");
		emit sendSlidervalue(value);
		});
	//connect(m_)
}
