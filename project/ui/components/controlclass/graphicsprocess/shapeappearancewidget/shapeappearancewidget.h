﻿#ifndef SHAPEAPPEARANCEWIDGET_H_
#define SHAPEAPPEARANCEWIDGET_H_
#include <QWidget>
class QVBoxLayout;
class QHBoxLayout;
class QLabel;
class QComboBox;
class QSlider;
class QLineEdit;
/// <summary>
/// 调整图形的外观
/// </summary>
class ShapeAppearanceWidget:public QWidget
{
	Q_OBJECT
public:
	ShapeAppearanceWidget(QWidget* parent = nullptr);
	~ShapeAppearanceWidget();
public slots:
	void setShapeTrans(const int&value);
signals:
	void sendSlidervalue(const int& value);

protected:
	void paintEvent(QPaintEvent* event);

private:
	void initWidget();
	void initSignals();


private:
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QLabel* m_functionname = nullptr;
	QLabel* m_opreationName = nullptr;
	QComboBox* m_comboBox = nullptr;
	QLineEdit* m_lineEdit = nullptr;
	QSlider* m_slider = nullptr;





};


#endif
