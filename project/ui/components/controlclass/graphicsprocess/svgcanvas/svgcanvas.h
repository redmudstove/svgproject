﻿#ifndef SVGCANVAS_H_
#define SVGCANVAS_H_
#include <QWidget>
#include <QPointF>
#include "kernel/shape/svgabstractshape.h"
#include <QList>
#include <QVector>
#include "components/selectshape/selectshape.h"
class keyPressEvent;

class SvgCanvas:public QWidget
{
	Q_OBJECT
public:
	SvgCanvas(QWidget* parent = nullptr);
	~SvgCanvas();

public slots:
	void isOpenGrid(bool flags);
	void setBackgroundColor(const QColor&);
	void setCurShape(const Shape::ShapeType shape);
	void changeSize(const QSize& size);
	void setScaling(const double& scaling);
	void zoomSize(const double& scaling, const QPointF& clickPos);
	void setSelectShapesTrans(const int& value);
	void setSelectShapesfillColor(const QColor& color);
	void setSelectShapsStrokeColor(const QColor& color);
protected:
	void setSelectedShape(const Shape::ShapeType shape);
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void wheelEvent(QWheelEvent* event) override;
	void paintEvent(QPaintEvent* event) override;
	void resizeEvent(QResizeEvent* event) override;
	void keyPressEvent(QKeyEvent* event) override;
	void keyReleaseEvent(QKeyEvent* event) override;
signals:
	void sendCanvasScaling(const double&scaling);
	//传递所选择图形的参数,后续需要添加序号。
	void sendShapeSelected(const int&index,const int&w, const int& h, const int& x, const int& y, 
		const int& ration,const int&trans,const int& blur,const QColor&fillcolor,const QColor&bordercolor);
	void sendSelectCanvas();
	void sendSlcaing(const double&sclaing);
private:
	void initWidget();
	void initSignals();
	void addShapeToCanvas(Shape::SvgAbstractShape* drawgraph);
	int shapeIsSelected(const QPointF& point);
	void selectedShapesMoveTo(const double& x, const double& y);
	void setSelectCursor(const SelectRegion::SelectPos& selectPos);
private:

	//set the background color
	QColor m_color;
	//鼠标是否按下
	bool isPressMouse;
	//此时是否是移动画布
	bool isMoveCanvas;
	bool isSpaceKeyPress;
	//移动画布的时候的鼠标按下
	bool isMovePress;
	//此时是否是处于绘制图形的过程中。
	bool isDrawCanvans;
	int m_indexSelected;

	int m_penWidth;
	QColor m_penColor;
	Qt::PenStyle m_penStyle;
	QColor m_fillColor;
	bool isDisableEdit;
	bool isStrchShape;
	int m_nums;

	QPointF m_stPos;
	QPointF m_endPos;
	QPointF m_lastPos;
	QSize m_size;
	SelectRegion m_surroundGraph;
	SelectRect m_selectRect;
	Grid m_grid;
	SelectRegion::SelectPos m_selectPos;

	//是否处于多选
	bool				m_isMultipleChoice;
	bool				m_multipleChoice;
	bool m_isDrawGrid;
	
private:
	double m_scaling;

	QVector<Shape::SvgAbstractShape*> m_shapeList;
	QVector<QPointF> m_path;
	Shape::ShapeType m_shape;
};



#endif
