#ifndef CANVASPROPERTYWIDGET_H_
#define CANVASPROPERTYWIDGET_H_
#include <QWidget>
class ColorDialog;
class QPushButton;
class QHBoxLayout;
class CanvasPropertyWidget:public QWidget
{
	Q_OBJECT
public:
	CanvasPropertyWidget(QWidget* parent = nullptr);
	~CanvasPropertyWidget();
protected:
	void paintEvent(QPaintEvent* event) override;

private:
	QPushButton* m_colorBtn = nullptr;



};



#endif
