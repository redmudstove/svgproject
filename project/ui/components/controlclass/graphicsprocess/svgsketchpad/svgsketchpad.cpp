﻿#include "components/controlclass/graphicsprocess/svgsketchpad/svgsketchpad.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
#include <QGridLayout>
#pragma execution_character_set("utf-8")
SvgSketchPad::SvgSketchPad(QWidget* parent /*= nullptr*/):QWidget(parent), m_scaling(1), m_isOpenruler(false)
{
	initWidget();
	initSignals();
}

SvgSketchPad::~SvgSketchPad()
{

}

void SvgSketchPad::isOpenGrid(bool flags)
{
	emit sendOpGrid(flags);
}

void SvgSketchPad::isOPenRuler(bool flags)
{
	m_isOpenruler = flags;
	update();

}

void SvgSketchPad::setCanvasSize(const int w, const int h)
{
	emit changeSvgcanvasSize(QSize(w, h));
}

void SvgSketchPad::setCurOPreation(const Shape::ShapeType shape)
{
	emit sendOPeationTodo(shape);
}

void SvgSketchPad::setShapesTransvalue(const int& value)
{

	emit sendShapesTransValue(value);
}

void SvgSketchPad::setShapesColor(const QColor& color)
{
	emit sendSelectColor(color);
}

void SvgSketchPad::setShapeStrokeColor(const QColor& color)
{
	emit sendSelectStrokeColor(color);
}

void SvgSketchPad::drawRuler(QPainter* painter)
{
	painter->save();
	painter->setPen(QPen(Qt::gray, 1));
	painter->setBrush(Qt::NoBrush);
	for (int i = 20; i <= this->width(); i += 5)
	{
		painter->save();
		painter->setPen(QColor(Qt::red));

		//painter.setBrush(QBrush(Qt::green,Qt::SolidPattern));
		QBrush brush;
		brush.setColor(Qt::green);
		brush.setStyle(Qt::SolidPattern);
		painter->setBrush(brush);
		painter->restore();
		if (i % 25 == 0) {
			painter->drawLine(QPointF(i, 0), QPointF(i, 20));
			painter->save();
			painter->setPen(QPen(Qt::red, 2));
			QFont font;
			font.setPointSizeF(5);
			painter->setFont(font);
			painter->drawText(QPointF(i - 3.0, 30), QString::number(i));

			painter->restore();
		}
		else
		{
			painter->drawLine(QPointF(i, 0), QPointF(i, 10));
		}

	}
	for (int i = 20; i <= this->height(); i += 5)
	{
		painter->save();
		painter->setPen(QColor(Qt::red));
		//painter.setBrush(QBrush(Qt::green,Qt::SolidPattern));
		QBrush brush;
		brush.setColor(Qt::green);
		brush.setStyle(Qt::SolidPattern);
		painter->setBrush(brush);
		painter->restore();
		if (i % 25 == 0) {
			painter->drawLine(QPointF(0, i), QPointF(20, i));
			painter->save();
			painter->setPen(QPen(Qt::red, 2));
			QFont font;
			font.setPointSizeF(5);
			painter->setFont(font);
			painter->drawText(QPointF(30, i + 5), QString::number(i));
			qDebug() << i;
			painter->restore();
		}
		else
		{
			painter->drawLine(QPointF(0, i), QPointF(10, i));
		}

	}
	painter->restore();
}

void SvgSketchPad::setScaling(const double& scaling)
{
	
}

void SvgSketchPad::paintEvent(QPaintEvent* event)
{
	/*QPainter temp(this);*/
	


	QStyleOption opt;
	QPainter p(this);
	opt.init(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	if (m_isOpenruler) {
		drawRuler(&p);
	}
	
	QWidget::paintEvent(event);

}

void SvgSketchPad::initWidget()
{

	m_vb = new QVBoxLayout(this);
	//m_hb = new QHBoxLayout;
	m_svgcanvasContainer = new SvgCanvasContainer(this);
	m_vb->addWidget(m_svgcanvasContainer);
	m_vb->setContentsMargins(50, 40, 10, 10);

}

void SvgSketchPad::initSignals()
{
	connect(this, &SvgSketchPad::sendOpGrid, m_svgcanvasContainer, &SvgCanvasContainer::isOpenGrid);
	connect(this, &SvgSketchPad::sendOPeationTodo, m_svgcanvasContainer, &SvgCanvasContainer::setCurShape);
	connect(this, &SvgSketchPad::changeSvgcanvasSize, m_svgcanvasContainer, &SvgCanvasContainer::changeSize);
	connect(m_svgcanvasContainer, &SvgCanvasContainer::sendSelectCanvas, this, [=]() {
		emit sendCanvasSelect();
		});
	connect(m_svgcanvasContainer, &SvgCanvasContainer::sendShapeSelected, this, [=](const int& index, const int& w, const int& h, const int& x, const int& y,
		const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor) {
			emit sendShapeSelected(index, w, h, x, y, ration, trans, blur, fillcolor, bordercolor);
		});
	connect(this, &SvgSketchPad::sendShapesTransValue, m_svgcanvasContainer, &SvgCanvasContainer::setSelectShapesTrans);
	connect(this, &SvgSketchPad::sendSelectColor, m_svgcanvasContainer, &SvgCanvasContainer::setSelectShapesfillColor);
	connect(this, &SvgSketchPad::sendSelectStrokeColor, m_svgcanvasContainer, &SvgCanvasContainer::setSelectShapsStrokeColor);
	//connect(m_)
}

void SvgSketchPad::OPenRuler(bool flags)
{
	/*if (flags == true) {
		m_hruler->show();
		m_vruler->show();
	}
	else
	{
		m_hruler->hide();
		m_vruler->hide();
	}*/

}

void SvgSketchPad::drawcanvasborderline(QPainter* p)
{
}
