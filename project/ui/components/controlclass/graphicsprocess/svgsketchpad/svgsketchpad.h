﻿#ifndef SVGSKETCHPAD_H_
#define SVGSKETCHPAD_H_
#include <QWidget>
#include "components/controlclass/graphicsprocess/svgdrawarearuler/svgruler.h"
#include "components/controlclass/graphicsprocess/svgcanvas/svgcanvas.h"
#include "kernel/shape/svgabstractshape.h"
#include <ui/components/workstationwidget/svgcanvascontainer.h>

//using namespace Ruler;
//画板
//包含标尺，滑动区域。
//using namespace Ruler;
class Ruler::SvgHRuler;
class Ruler::SvgVRuler;
class QHBoxLayout;
class QVBoxLayout;
class QGridLayout;

class SvgSketchPad:public QWidget
{
	Q_OBJECT
public:
	SvgSketchPad(QWidget* parent = nullptr);
	~SvgSketchPad();

public slots:
	void isOpenGrid(bool flags);
	void isOPenRuler(bool flags);
	void setCanvasSize(const int w, const int h);
	void setCurOPreation(const Shape::ShapeType shape);
	void setShapesTransvalue(const int& value);
	void setShapesColor(const QColor&color);
	void setShapeStrokeColor(const QColor& color);
	void drawRuler(QPainter* painter);
	void setScaling(const double& scaling);
signals:
	//void sendSlcaing(const double& sclaing);
	void sendOpGrid(bool flags);
	void sendOPeationTodo(const Shape::ShapeType shape);
	void changeSvgcanvasSize(const QSize& size);
	void sendCanvasSelect();
	void sendShapeSelected(const int& index, const int& w, const int& h, const int& x, const int& y,
		const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor);
	void sendShapesTransValue(const int& value);
	void sendSelectColor(const QColor&color);
	void sendSelectStrokeColor(const QColor& color);
protected:
	void paintEvent(QPaintEvent* event) override;

private:
	void initWidget();
	void initSignals();
	void OPenRuler(bool flags);
	void drawcanvasborderline(QPainter* p);
private:
	
	QHBoxLayout* m_hb = nullptr;
	QHBoxLayout* m_hb1 = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QGridLayout* m_layout = nullptr;
	SvgCanvasContainer* m_svgcanvasContainer = nullptr;
	bool isOpengrid;
	double m_scaling;
	bool m_isOpenruler;
};
//template <class type,int dim>
//class POint {
//public:
//	POint();
//	POint(type coords[dim]) {
//		for (int index = 0; index < dim; index++) {
//			_coords[index] = coords[index];
//		};
//		type& operator[](int index) {
//			assert(index < dim&& index >= 0);
//			return _coords[index];
//		}
//		type operator[](int index) const {}
//
//	}
//private:
//	type _coords[dim];
//
//};

#endif // !SVGSKETCHPAD_H_
