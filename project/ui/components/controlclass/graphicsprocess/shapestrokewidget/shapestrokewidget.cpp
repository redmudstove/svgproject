﻿#include "components/controlclass/graphicsprocess/shapestrokewidget/shapestrokewidget.h"
#include "colordialog.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QStyleOption>
#include <QPainter>
#pragma execution_character_set("utf-8")
ShapeStrokeWidget::ShapeStrokeWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

ShapeStrokeWidget::~ShapeStrokeWidget()
{

}

void ShapeStrokeWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void ShapeStrokeWidget::initWidget()
{
	m_vb = new QVBoxLayout;
	m_functionName = new QLabel(this);
	m_functionName->setFixedSize(40, 30);
	m_functionName->setText(tr("描边"));
	m_functionName->setProperty("shapeprarameterlab", "true");
	m_hb1 = new QHBoxLayout;
	m_hb1->addWidget(m_functionName);
	m_hb1->addStretch();
	m_hb1->setContentsMargins(10, 0, 0, 0);

	m_hb2 = new QHBoxLayout;
	m_colorBtn = new QPushButton(this);
	m_colorBtn->setFixedSize(30, 30);
	m_colorBtn->setProperty("shapeprarameter", "true");
	m_lineStyle = new QComboBox(this);
	m_lineStyle->setFixedSize(80, 30);
	m_lineStyle->addItem(tr("居中"));
	m_lineStyle->addItem(tr("外部"));
	m_lineStyle->addItem(tr("内部"));
	m_lineStyle->setCurrentIndex(0);

	m_borderEdt = new QLineEdit(this);
	m_borderEdt->setFixedSize(80, 30);
	m_borderEdt->setProperty("shapeedit", "true");
	m_hb2->addWidget(m_colorBtn);
	m_hb2->addWidget(m_lineStyle);
	m_hb2->addWidget(m_borderEdt);
	m_hb2->addStretch();
	m_hb2->setSpacing(20);
	m_hb2->setContentsMargins(32, 0, 0, 0);
	m_vb->addLayout(m_hb1);
	m_vb->addLayout(m_hb2);

	this->setLayout(m_vb);
	m_colorDialog = new ColorDialog(this);
	m_colorDialog->setWindowFlag(Qt::Popup);
	m_colorDialog->hide();
}

void ShapeStrokeWidget::initSignals()
{
	connect(m_colorBtn, &QPushButton::clicked, this, [=]() {
		m_colorDialog->move(m_colorBtn->mapToGlobal(QPoint(QPoint(0, 0).x() - 350, QPoint(0, 0).y() - 200)));
		m_colorDialog->show();
		});
	connect(m_colorDialog, &ColorDialog::sendSelectColor, this, [=](const QColor& color) {
		emit sendSelectStrokeColor(color);

		});
}

