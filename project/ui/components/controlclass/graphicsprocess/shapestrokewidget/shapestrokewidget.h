﻿#ifndef SHAPESTROKEWIDGET_H_
#define	SHAPESTROKEWIDGET_H_
#include <QWidget>
class QLabel;
class QPushButton;
class QHBoxLayout;
class QVBoxLayout;
class QLineEdit;
class ColorDialog;
class QComboBox;
class ColorDialog;
/// <summary>
/// 图形的边框属性界面
/// </summary>
class ShapeStrokeWidget :public QWidget {
	Q_OBJECT
public:
	ShapeStrokeWidget(QWidget* parent = nullptr);
	~ShapeStrokeWidget();

signals:
	void sendSelectStrokeColor(const QColor& color);
protected:

	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initSignals();

private:
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QVBoxLayout* m_vb = nullptr; 
	QLabel* m_functionName = nullptr;
	QPushButton* m_colorBtn = nullptr;
	QLineEdit* m_borderEdt = nullptr;
	QComboBox* m_lineStyle = nullptr;

	ColorDialog* m_colorDialog = nullptr;
};




#endif
