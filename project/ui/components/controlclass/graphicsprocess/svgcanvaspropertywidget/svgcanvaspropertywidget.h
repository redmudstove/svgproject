#ifndef SVGCANVASPROPERTYWIDGET_H_
#define SVGCANVASPROPERTYWIDGET_H_
#include "ui/components/controlclass/slidlabel/slidlabel.h"
#include <QWidget>
class QVBoxLayout;
class QHBoxLayout;
class QLineEdit;
class QComboBox;
class QPushButton;
class ColorDialog;


class SvgCanvasProWidget:public QWidget
{
	Q_OBJECT
public:
	SvgCanvasProWidget(QWidget* parent = nullptr);
	~SvgCanvasProWidget();
protected:
	void paintEvent(QPaintEvent* event) override;
signals:
	void sendCanvasSize(const int w,const int h);
private:
	void initWidget();
	void initSignals();
private:
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QLineEdit* m_widthEdit = nullptr;
	QLineEdit* m_heightEdit = nullptr;
	QLineEdit* m_colorEdit = nullptr;
	QComboBox* m_sizecmb = nullptr;
	SlidLabel* m_widthlbl = nullptr;
	SlidLabel* m_heightlbl = nullptr;
	//SlidLabel* m_sizelbl = nullptr;
	QLabel* m_sizelbl = nullptr;
	QPushButton* m_colorBtn = nullptr;
	ColorDialog* m_colorDia = nullptr;

};
#endif
