﻿#include "components/controlclass/graphicsprocess/svgcanvaspropertywidget/svgcanvaspropertywidget.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "ui/components/controlclass/colordialog/colordialog.h"
#include <QLineEdit>
#include <QComboBox>
#include <QDebug>
#pragma execution_character_set("utf-8")
SvgCanvasProWidget::SvgCanvasProWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	m_colorDia = new ColorDialog(this);
	m_colorDia->hide();
	m_colorDia->setWindowFlag(Qt::Popup);
	initWidget();
	initSignals();
}

SvgCanvasProWidget::~SvgCanvasProWidget()
{

}

void SvgCanvasProWidget::paintEvent(QPaintEvent* event)
{

}

void SvgCanvasProWidget::initWidget()
{
	m_vb = new QVBoxLayout(this);
	m_hb1 = new QHBoxLayout;
	m_hb2 = new QHBoxLayout;

	m_widthlbl = new SlidLabel(this);
	m_widthlbl->setFixedSize(40, 30);
	m_widthlbl->setText(tr("宽度"));
	m_widthEdit = new QLineEdit(this);
	m_widthEdit->setFixedSize(60, 30);
	m_widthEdit->setText(tr("500"));
	m_hb1->addWidget(m_widthlbl);
	m_hb1->addWidget(m_widthEdit);

	m_heightlbl = new SlidLabel(this);
	m_heightlbl->setText(tr("长度"));
	m_heightlbl->setFixedSize(40, 30);
	m_heightEdit = new QLineEdit(this);
	m_heightEdit->setText(tr("500"));
	m_heightEdit->setFixedSize(60, 30);
	m_hb1->addStretch();
	m_hb1->addWidget(m_heightlbl);
	m_hb1->addWidget(m_heightEdit);

	m_colorBtn = new QPushButton(this);
	m_colorBtn->setFixedSize(30, 30);
	m_colorEdit = new QLineEdit(this);
	m_colorEdit->setFixedSize(60, 30);
	m_sizelbl = new QLabel(this);
	m_sizelbl->setText(tr("尺寸"));
	m_sizelbl->setFixedSize(40, 30);

	m_sizecmb = new QComboBox(this);
	m_sizecmb->setFixedSize(100, 40);
	m_sizecmb->addItem(tr("500 X 500"));
	m_sizecmb->addItem(tr("800 X 600"));
	m_sizecmb->addItem(tr("1200 X 800"));

	m_sizecmb->setCurrentIndex(0);
	m_hb2->addWidget(m_colorBtn);
	m_hb2->setSpacing(10);
	m_hb2->addWidget(m_colorEdit);
	m_hb2->addStretch();
	//m_hb2->setSpacing(10);
	m_hb2->addWidget(m_sizelbl);
	m_hb2->addWidget(m_sizecmb);
	m_hb1->setContentsMargins(10, 2, 50, 2);
	m_hb2->setContentsMargins(10, 2, 5, 2);
	m_vb->addLayout(m_hb1);
	m_vb->addLayout(m_hb2);
	m_vb->addStretch();
	m_vb->setSpacing(1);
	m_vb->setContentsMargins(0, 10, 0, 0);
	m_colorBtn->setProperty("shapeprarameter", "true");

	m_heightlbl->setProperty("shapeprarameterlab", "true");
	m_widthlbl->setProperty("shapeprarameterlab", "true");
	m_sizelbl->setProperty("shapeprarameterlab", "true");
	m_colorEdit->setProperty("shapeedit", "true");
	m_widthEdit->setProperty("shapeedit", "true");
	m_heightEdit->setProperty("shapeedit", "true");
}

void SvgCanvasProWidget::initSignals()
{
	connect(m_widthlbl, &SlidLabel::wheelslip, [=](const bool& direction) {
		
		if (direction) {

			if (3000 > m_widthEdit->text().toInt())
				m_widthEdit->setText(QString::number(m_widthEdit->text().toInt() + 10));
			emit sendCanvasSize(m_widthEdit->text().toInt(), m_heightEdit->text().toInt());
		}
		else
		{
			if (10 < m_widthEdit->text().toInt())
			{
				m_widthEdit->setText(QString::number(m_widthEdit->text().toInt() - 10));
				emit sendCanvasSize(m_widthEdit->text().toInt(), m_heightEdit->text().toInt());
			}
		}

		});

	connect(m_heightlbl, &SlidLabel::wheelslip, [=](const bool& direction) {
		
		if (direction) {

			if (3000 > m_heightEdit->text().toInt())
				m_heightEdit->setText(QString::number(m_heightEdit->text().toInt() + 10));
			emit sendCanvasSize(m_widthEdit->text().toInt(), m_heightEdit->text().toInt());
		}
		else
		{
			if (10 < m_heightEdit->text().toInt())
			{
				m_heightEdit->setText(QString::number(m_heightEdit->text().toInt() - 10));
				emit sendCanvasSize(m_widthEdit->text().toInt(), m_heightEdit->text().toInt());
			}
		}

		});

	connect(m_sizecmb, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](int index) {
		if (index == 0) {
			m_widthEdit->setText("500");
			m_heightEdit->setText("500");
			emit sendCanvasSize(m_widthEdit->text().toInt(), m_heightEdit->text().toInt());
		}else if(index==1){
			m_widthEdit->setText("800");
			m_heightEdit->setText("600");
			emit sendCanvasSize(m_widthEdit->text().toInt(), m_heightEdit->text().toInt());
		}
		else if (index == 2) {
			m_widthEdit->setText("1200");
			m_heightEdit->setText("800");
			emit sendCanvasSize(m_widthEdit->text().toInt(), m_heightEdit->text().toInt());
		}
		});
	connect(m_colorBtn, &QPushButton::clicked, this, [=]() {
		if (m_colorDia->isVisible()) {
			m_colorDia->hide();
		}
		else
		{
			m_colorDia->move(m_colorBtn->mapToGlobal(QPoint(QPoint(0, 0).x() - 320, QPoint(0, 0).y() - 200)));
			m_colorDia->show();
		}
		
		});
}

