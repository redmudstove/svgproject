#include "components/controlclass/graphicsprocess/svgdrawarearuler/svgruler.h"
#include <QPainter>
#include <QDebug>
#include <QStyleOption>
Ruler::SvgHRuler::SvgHRuler(QWidget* parent /*= nullptr*/):QWidget(parent)
	,m_mousePos(QPointF(0,0))
	,m_stPos(0.0)
	,m_enPos(0.0)
	,m_scaler(0)
{
	setMouseTracking(true);
	//this->setWindowFlag(Qt::FramelessWindowHint);
	this->setFixedHeight(35);
}

Ruler::SvgHRuler::~SvgHRuler()
{

}

void Ruler::SvgHRuler::setScaler(double s)
{
	this->m_scaler = s;
}

void Ruler::SvgHRuler::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	drawBackground(&painter);
	QStyleOption opt;
	opt.init(this);
	//QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
	QWidget::paintEvent(event);
	painter.end();
}

void Ruler::SvgHRuler::drawBackground(QPainter* painter)
{
	painter->save();
	painter->setPen(QPen(Qt::gray, 1));
	painter->setBrush(Qt::NoBrush);
	for (int i = 0; i <= this->width(); i += 5)
	{
		painter->save();
		painter->setPen(QColor(Qt::red));

		//painter.setBrush(QBrush(Qt::green,Qt::SolidPattern));
		QBrush brush;
		brush.setColor(Qt::green);
		brush.setStyle(Qt::SolidPattern);
		painter->setBrush(brush);
		painter->drawRect(QRectF(QPointF(m_mousePos.x(), 0), QPointF(m_mousePos.x() + 10, 10)));
		painter->restore();
		if (i % 25 == 0) {
			painter->drawLine(QPointF(i, 0), QPointF(i, 20));
			painter->save();
			painter->setPen(QPen(Qt::red, 2));
			QFont font;
			font.setPointSizeF(5);
			painter->setFont(font);
			painter->drawText(QPointF(i - 3.0, 30), QString::number(i));

			painter->restore();
		}
		else
		{
			painter->drawLine(QPointF(i, 0), QPointF(i, 10));
		}

	}
	painter->restore();
}

void Ruler::SvgHRuler::drawSelectArea(QPainter* painter)
{

}

Ruler::SvgVRuler::SvgVRuler(QWidget* parent /*= nullptr*/):QWidget(parent)
, m_mousePos(QPointF(0, 0))
, m_stPos(0.0)
, m_enPos(0.0)
, m_scaler(0)
{
	
	setMouseTracking(true);
	//this->setWindowFlag(Qt::FramelessWindowHint);
	this->setFixedWidth(45);
}

Ruler::SvgVRuler::~SvgVRuler()
{

}

void Ruler::SvgVRuler::setSelectwidth(int s, int e)
{

}

void Ruler::SvgVRuler::setScaler(double s)
{
	m_scaler = s;
}

void Ruler::SvgVRuler::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	drawBackground(&painter);
	painter.end();
}

void Ruler::SvgVRuler::drawBackground(QPainter* painter)
{
	painter->save();
	painter->setPen(QPen(Qt::gray, 1));
	painter->setBrush(Qt::NoBrush);
	for (int i = 0; i <= this->height(); i += 5)
	{
		painter->save();
		painter->setPen(QColor(Qt::red));

		//painter.setBrush(QBrush(Qt::green,Qt::SolidPattern));
		QBrush brush;
		brush.setColor(Qt::green);
		brush.setStyle(Qt::SolidPattern);
		painter->setBrush(brush);
		painter->drawRect(QRectF(QPointF(m_mousePos.x(), 0), QPointF(m_mousePos.x() + 10, 10)));
		painter->restore();
		if (i % 25 == 0) {
			painter->drawLine(QPointF(0, i), QPointF(20, i));
			painter->save();
			painter->setPen(QPen(Qt::red, 2));
			QFont font;
			font.setPointSizeF(5);
			painter->setFont(font);
			painter->drawText(QPointF(30, i+5) , QString::number(i));
			qDebug() << i;
			painter->restore();
		}
		else
		{
			painter->drawLine(QPointF(0, i), QPointF(10, i));
		}

	}
	painter->restore();
}

void Ruler::SvgVRuler::drawSelectArea(QPainter* painter)
{

}
