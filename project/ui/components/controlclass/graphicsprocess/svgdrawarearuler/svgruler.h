﻿#ifndef SVGRULER_H_
#define SVGRULER_H_
#include <QWidget>
#include <QPointF>
/// <summary>
/// 标尺
/// 第一版，只有缩放的功能
/// </summary>
namespace Ruler{
	class SvgHRuler :public QWidget
	{
		Q_OBJECT
	public:
		SvgHRuler(QWidget* parent = nullptr);
		~SvgHRuler();

	public slots:
		void setScaler(double);
	protected:
		void paintEvent(QPaintEvent* event) override;

	private:
		void drawBackground(QPainter* painter);
		void drawSelectArea(QPainter* painter);
	private:

		QPointF m_mousePos;
		double m_stPos;
		double m_enPos;
		double m_scaler;
	};

	class SvgVRuler:public QWidget
	{
		Q_OBJECT
	public:
		SvgVRuler(QWidget* parent = nullptr);
		~SvgVRuler();
	public slots:
		void setSelectwidth(int s, int e);
		void setScaler(double);
	protected:
		void paintEvent(QPaintEvent* event) override;
	private:
		void drawBackground(QPainter* painter);
		void drawSelectArea(QPainter* painter);

	private:
		QPointF m_mousePos;
		double m_stPos;
		double m_enPos;
		double m_scaler;

	};
}

#endif
