#include "components/controlclass/graphicsprocess/shapeparameterwidget/shapeparameterwidget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
ShapeParameterWidget::ShapeParameterWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
}

ShapeParameterWidget::~ShapeParameterWidget()
{

}

void ShapeParameterWidget::setParameter(const int&w, const int&h, const int&x, const int&y, const int& ra)
{
	m_wlineEdt->setText(QString::number(w));
	m_hlineEdt->setText(QString::number(h));
	m_xlineEdt->setText(QString::number(x));
	m_ylineEdt->setText(QString::number(y));
	m_dlineEdt->setText(QString::number(ra));

}

void ShapeParameterWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void ShapeParameterWidget::initWidget()
{
	m_hb1 = new QHBoxLayout;
	m_x = new QLabel(this);
	m_x->setFixedSize(20, 30);
	m_x->setText(tr("X"));
	m_y = new QLabel(this);
	m_y->setFixedSize(20, 30);
	m_y->setText(tr("Y"));
	m_xlineEdt = new QLineEdit(this);
	m_xlineEdt->setFixedSize(60, 30);

	m_ylineEdt = new QLineEdit(this);
	m_ylineEdt->setFixedSize(60, 30);

	m_hb1->addWidget(m_x);
	m_hb1->addWidget(m_xlineEdt);
	m_hb1->addStretch();
	//m_hb1->setSpacing(2);
	m_hb1->addWidget(m_y);
	m_hb1->addWidget(m_ylineEdt);
	m_hb1->setSpacing(2);
	m_hb1->setContentsMargins(10, 2, 50, 2);


	m_w = new QLabel(this);
	m_w->setFixedSize(20, 30);
	m_w->setText(tr("W"));
	m_h = new QLabel(this);
	m_h->setFixedSize(20, 30);
	m_h->setText(tr("H"));
	m_wlineEdt = new QLineEdit(this);
	m_wlineEdt->setFixedSize(60, 30);
	m_hlineEdt = new QLineEdit(this);
	m_hlineEdt->setFixedSize(60, 30);

	m_hb2 = new QHBoxLayout;
	m_hb2->addWidget(m_w);
	m_hb2->addWidget(m_wlineEdt);
	m_hb2->addStretch();
	m_hb2->addWidget(m_h);
	m_hb2->addWidget(m_hlineEdt);
	m_hb2->setSpacing(2);
	m_hb2->setContentsMargins(10, 2, 50, 2);

	m_Hb3 = new QHBoxLayout;
	m_rotateBtn = new QPushButton(this);
	m_rotateBtn->setIconSize(QSize(24, 24));
	m_rotateBtn->setIcon(QIcon(":/shapeproperty/res/icons/ui/control/shapeproperty/rotate.png"));
	m_rotateBtn->setFixedSize(30, 30);
	m_dlineEdt = new QLineEdit(this);
	m_dlineEdt->setFixedSize(60, 30);
	m_fliphBtn = new QPushButton(this);
	m_fliphBtn->setIconSize(QSize(24,24));
	m_fliphBtn->setIcon(QIcon(":/shapeproperty/res/icons/ui/control/shapeproperty/updownturn1.png"));
	m_fliphBtn->setFixedSize(30, 30);
	m_flipvBtn = new QPushButton(this);
	m_flipvBtn->setIconSize(QSize(24, 24));
	m_flipvBtn->setIcon(QIcon(":/shapeproperty/res/icons/ui/control/shapeproperty/updownturn2.png"));
	m_flipvBtn->setFixedSize(30, 30);
	
	m_Hb3->addWidget(m_rotateBtn);

	m_Hb3->addWidget(m_dlineEdt);
	m_Hb3->addWidget(m_fliphBtn);
	m_Hb3->addWidget(m_flipvBtn);
	m_Hb3->setSpacing(2);
	m_rotateBtn->setProperty("shapeprarameter","true");
	m_flipvBtn->setProperty("shapeprarameter", "true");
	m_fliphBtn->setProperty("shapeprarameter", "true");
	//m_Hb3->addStretch();
	m_Hb3->setContentsMargins(10, 2, 20, 2);

	m_x->setProperty("shapeprarameterlab", "true");
	m_y->setProperty("shapeprarameterlab", "true");
	m_h->setProperty("shapeprarameterlab", "true");
	m_w->setProperty("shapeprarameterlab", "true");

	m_hlineEdt->setProperty("shapeedit", "true");
	m_dlineEdt->setProperty("shapeedit", "true");
	m_xlineEdt->setProperty("shapeedit", "true");
	m_ylineEdt->setProperty("shapeedit", "true");
	m_wlineEdt->setProperty("shapeedit", "true");

	m_vb = new QVBoxLayout;
	m_vb->addLayout(m_hb1);
	m_vb->addLayout(m_hb2);
	m_vb->addLayout(m_Hb3);
	m_vb->setSpacing(5);
	this->setLayout(m_vb);

}

void ShapeParameterWidget::initSignals()
{

}

