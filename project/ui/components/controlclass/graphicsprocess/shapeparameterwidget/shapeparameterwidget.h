﻿#ifndef SHAPEPARAMETERWIDGET_H_
#define SHAPEPARAMETERWIDGET_H_
#include <QWidget>
class QPushButton;
class QLabel;
class QLineEdit;
class QHBoxLayout;
class QVBoxLayout;
/// <summary>
/// 调整参数的界面
/// </summary>
class ShapeParameterWidget:public QWidget
{
	Q_OBJECT
public:
	ShapeParameterWidget(QWidget* parent = nullptr);
	~ShapeParameterWidget();

public slots:
	void setParameter(const int&,const int&,const int&,const int&,const int&);
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initSignals();

private:
	QLabel* m_x = nullptr;
	QLabel* m_y = nullptr;
	QLabel* m_w = nullptr;
	QLabel* m_h = nullptr;
	QPushButton* m_rotateBtn = nullptr;
	QPushButton* m_fliphBtn = nullptr;
	QPushButton* m_flipvBtn = nullptr;
	QLineEdit* m_xlineEdt = nullptr;
	QLineEdit* m_ylineEdt = nullptr;
	QLineEdit* m_wlineEdt = nullptr;
	QLineEdit* m_hlineEdt = nullptr;
	QLineEdit* m_dlineEdt = nullptr;
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QHBoxLayout* m_Hb3 = nullptr;
	QVBoxLayout* m_vb = nullptr;



};



#endif // !SHAPEPARAMETERWIDGET_H_
