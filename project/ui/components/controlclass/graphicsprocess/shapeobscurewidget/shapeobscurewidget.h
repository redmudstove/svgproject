#ifndef SHAPEOBSCUREWIDGET_H_
#define SHAPEOBSCUREWIDGET_H_
#include <QWidget>
class QPushButton;
class QLabel;
class QLineEdit;
class QSlider;
class QComboBox;
class QHBoxLayout;
class QVBoxLayout;
#pragma execution_character_set("utf-8")
class ShapeObscureWidget:public QWidget
{
	Q_OBJECT
public:
	ShapeObscureWidget(QWidget* parent = nullptr);
	~ShapeObscureWidget();
protected:
	void paintEvent(QPaintEvent* event) override;

private:
	void initWidget();
	void initSignals();
private:
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QLabel* m_fucntionName = nullptr;
	QComboBox* m_dealWay = nullptr;
	QSlider* m_dealslider = nullptr;
	QLineEdit* m_lineWay = nullptr;

};


#endif
