﻿#include "components/controlclass/graphicsprocess/shapeobscurewidget/shapeobscurewidget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QLineEdit>
#include <QComboBox>
#include <QLabel>
#include <QStyleOption>
#include <QPainter>
ShapeObscureWidget::ShapeObscureWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
}

ShapeObscureWidget::~ShapeObscureWidget()
{

}

void ShapeObscureWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void ShapeObscureWidget::initWidget()
{
	m_vb = new QVBoxLayout;
	m_hb1 = new QHBoxLayout;
	m_fucntionName = new QLabel(this);
	m_fucntionName->setFixedSize(40, 30);
	m_fucntionName->setText(tr("模糊"));
	m_fucntionName->setProperty("shapeprarameterlab", "true");
	m_hb1->addWidget(m_fucntionName);
	m_hb1->addStretch();
	m_hb1->setContentsMargins(16, 0, 0, 0);

	m_hb2 = new QHBoxLayout;
	m_dealWay = new QComboBox(this);
	m_dealWay->setFixedSize(90, 30);
	m_dealWay->addItem(tr("高斯模糊"));
	m_dealWay->addItem(tr("背景模糊"));
	m_dealslider = new QSlider(Qt::Horizontal,this);
	m_dealslider->setFixedSize(120, 30);
	m_lineWay = new QLineEdit(this);
	m_lineWay->setFixedSize(40, 30);
	m_lineWay->setProperty("shapeedit", "true");

	m_hb2->addWidget(m_dealWay);
	m_hb2->addWidget(m_dealslider);
	m_hb2->addWidget(m_lineWay);
	m_hb2->addStretch();
	m_hb2->setContentsMargins(16, 0, 0, 5);
	m_vb->addLayout(m_hb1);
	m_vb->addLayout(m_hb2);
	m_vb->addStretch();
	m_vb->setContentsMargins(5, 0, 0, 5);

	this->setLayout(m_vb);


}

void ShapeObscureWidget::initSignals()
{

}

