#include "components/controlclass/graphicsprocess/shapealignwidget/shapealignwidget.h"
#include <QHBoxLayout>
#include <QPushButton>
#include <QStyleOption>
#include <QPainter>
ShapeAlignWidget::ShapeAlignWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
}

ShapeAlignWidget::~ShapeAlignWidget()
{

}

void ShapeAlignWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter painter(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
	QWidget::paintEvent(event);
}

void ShapeAlignWidget::enterEvent(QEvent* event)
{
	this->setCursor(Qt::PointingHandCursor);
	QWidget::enterEvent(event);
}

void ShapeAlignWidget::initWidget()
{
	m_hb = new QHBoxLayout;

	m_allalignBtn = new QPushButton(this);
	m_allalignBtn->setFixedSize(20, 32);
	m_allalignBtn->setIconSize(QSize(20,20));
	m_allalignBtn->setIcon(QIcon(":/align/res/icons/ui/control/alignbtn/more.png"));

	m_leftAlignBtn = new QPushButton(this);
	m_leftAlignBtn->setFixedSize(32, 32);
	m_leftAlignBtn->setIconSize(QSize(24,24));
	m_leftAlignBtn->setIcon(QIcon(":/align/res/icons/ui/control/alignbtn/letfalignnormal.png"));

	m_hcenterAlignBtn = new QPushButton(this);
	m_hcenterAlignBtn->setIconSize(QSize(24, 24));
	m_hcenterAlignBtn->setIcon(QIcon(":/align/res/icons/ui/control/alignbtn/centeralignnormal.png"));
	m_hcenterAlignBtn->setFixedSize(32, 32);

	m_rightAlignBtn = new QPushButton(this);
	m_rightAlignBtn->setIconSize(QSize(24, 24));
	m_rightAlignBtn->setIcon(QIcon(":/align/res/icons/ui/control/alignbtn/rightalignnormal.png"));
	m_rightAlignBtn->setFixedSize(32, 32);


	m_topAlignBtn = new QPushButton(this);
	m_topAlignBtn->setFixedSize(32, 32);
	m_topAlignBtn->setIconSize(QSize(32, 32));
	m_topAlignBtn->setIcon(QIcon(":/align/res/icons/ui/control/alignbtn/topalignnormal.png"));

	m_vcenterAlignBtn = new QPushButton(this);
	m_vcenterAlignBtn->setIconSize(QSize(32, 32));
	m_vcenterAlignBtn->setIcon(QIcon(":/align/res/icons/ui/control/alignbtn/verticalalign.png"));
	m_vcenterAlignBtn->setFixedSize(32, 32);


	m_bottomAlignBtn = new QPushButton(this);
	m_bottomAlignBtn->setIconSize(QSize(32, 32));
	m_bottomAlignBtn->setIcon(QIcon(":/align/res/icons/ui/control/alignbtn/bottomalignnormal.png"));
	m_bottomAlignBtn->setFixedSize(32, 32);
	m_hb->addWidget(m_allalignBtn);
	m_hb->addWidget(m_leftAlignBtn);
	m_hb->addWidget(m_hcenterAlignBtn);
	m_hb->addWidget(m_rightAlignBtn);
	m_hb->addWidget(m_topAlignBtn);
	m_hb->addWidget(m_vcenterAlignBtn);
	m_hb->addWidget(m_bottomAlignBtn);
	m_hb->setSpacing(2);
	m_hb->setContentsMargins(2, 2, 2, 2);
	m_allalignBtn->setProperty("shapealignbtn", "true");
	m_leftAlignBtn->setProperty("shapealignbtn", "true");
	m_hcenterAlignBtn->setProperty("shapealignbtn", "true");
	m_rightAlignBtn->setProperty("shapealignbtn", "true");
	m_topAlignBtn->setProperty("shapealignbtn", "true");
	m_vcenterAlignBtn->setProperty("shapealignbtn", "true");
	m_bottomAlignBtn->setProperty("shapealignbtn", "true");
	this->setLayout(m_hb);
	

}

void ShapeAlignWidget::initSignals()
{

}

