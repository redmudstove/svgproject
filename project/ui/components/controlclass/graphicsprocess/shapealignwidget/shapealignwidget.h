﻿#ifndef SHAPEALIGNWIDGET_H_
#define SHAPEALIGNWIDGET_H_
#include <QWidget>
class QHBoxLayout;
class QPushButton;
/// <summary>
/// 对齐的功能界面
/// </summary>
class ShapeAlignWidget:public QWidget	
{
	Q_OBJECT
public:
	ShapeAlignWidget(QWidget* parent = nullptr);
	~ShapeAlignWidget();
protected:
	void paintEvent(QPaintEvent* event) override;
	void enterEvent(QEvent* event) override;

private:
	void initWidget();
	void initSignals();
private:
	QHBoxLayout* m_hb = nullptr;
	QPushButton* m_leftAlignBtn = nullptr;
	QPushButton* m_vcenterAlignBtn = nullptr;
	QPushButton* m_rightAlignBtn = nullptr;
	QPushButton* m_topAlignBtn = nullptr;
	QPushButton* m_bottomAlignBtn = nullptr;
	QPushButton* m_hcenterAlignBtn = nullptr;
	QPushButton* m_allalignBtn = nullptr;

};

#endif