﻿#include "components/controlclass/graphicsprocess/shapefillwidget/shapefillwidget.h"
#include "components/controlclass/colordialog/colordialog.h"
#include "components/controlclass/colordialog/colordialog.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
#pragma execution_character_set("utf-8")
ShapeFillWidget::ShapeFillWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

ShapeFillWidget::~ShapeFillWidget()
{

}

void ShapeFillWidget::setShapefillParameter(const QColor& color, const int& trans)
{
	updateLineValue(color);
	m_hueEdt->setText(QString::number((trans / 255.00) * 100, 'd', 0) + "  %");
}

void ShapeFillWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void ShapeFillWidget::initWidget()
{
	m_hb1 = new QHBoxLayout;
	m_functionName = new QLabel(this);
	m_functionName->setText(tr("填充"));
	m_functionName->setFixedSize(40, 30);
	m_functionName->setProperty("shapeprarameterlab", "true");
	m_hb1->addWidget(m_functionName);
	m_hb1->addStretch();
	m_hb1->setContentsMargins(16, 0, 0, 0);

	m_hb2 = new QHBoxLayout;
	m_colorBtn = new QPushButton(this);
	m_colorBtn->setFixedSize(30, 30);
	m_colorEdt = new QLineEdit(this);
	/*m_colorEdt->addAction(QIcon(":/align/res/icons/ui/control/shapeproperty/percent.png"),QLineEdit::LeadingPosition);*/
	m_colorEdt->setFixedSize(80, 30);
	m_colorBtn->setProperty("shapeprarameter", "true");
	m_colorEdt->setProperty("shapeedit", "true");
	m_colorEdt->setText("#");
	m_colorEdt->setAlignment(Qt::AlignLeft);
	m_hueEdt = new QLineEdit(this);
	m_hueEdt->setFixedSize(80, 30);
	/*m_hueEdt->addAction(QIcon(":/align/res/icons/ui/control/shapeproperty/percent.png"),QLineEdit::TrailingPosition);*/
	m_hueEdt->setProperty("shapeedit", "true");

	m_hueEdt->setText("%");
	m_hueEdt->setAlignment(Qt::AlignRight);

	m_hb2->addWidget(m_colorBtn);
	m_hb2->addWidget(m_colorEdt);
	m_hb2->addWidget(m_hueEdt);
	m_hb2->setSpacing(20);
	m_hb2->addStretch();
	m_hb2->setContentsMargins(40, 0, 0, 0);

	m_vb = new QVBoxLayout;
	m_vb->addLayout(m_hb1);
	m_vb->addLayout(m_hb2);
	m_vb->setContentsMargins(5, 0, 0, 10);
	this->setLayout(m_vb);
	m_colorDialog = new ColorDialog(this);
	m_colorDialog->setWindowFlag(Qt::Popup);
	m_colorDialog->hide();
}

void ShapeFillWidget::initSignals()
{
	connect(m_colorBtn, &QPushButton::clicked, this, [=]() {
		m_colorDialog->move(m_colorBtn->mapToGlobal(QPoint(QPoint(0, 0).x() - 350, QPoint(0, 0).y() - 200)));
		m_colorDialog->show();
		});
	connect(m_colorDialog, &ColorDialog::sendSelectColor, this, [=](const QColor& color) {
		emit sendSelectColor(color);
		updateLineValue(color);
		});

}

void ShapeFillWidget::updateLineValue(const QColor&color)
{
	QString strR = QString::number(color.red(), 16);
	QString strG = QString::number(color.green(), 16);
	QString strB = QString::number(color.blue(), 16);
	QString strRgb = QString("%1%2%3").arg(QString("%1").arg(strR.size() == 1 ? strR.prepend("0") : strR),
		QString("%1").arg(strG.size() == 1 ? strG.prepend("0") : strG),
		QString("%1").arg(strB.size() == 1 ? strB.prepend("0") : strB));
	m_hueEdt->setText(QString::number((color.alpha() / 255.00) * 100, 'd', 0)+"%");
	m_colorEdt->setText("#" + strRgb);
}

