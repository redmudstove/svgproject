﻿#ifndef SHAPEFILLWIDGET_H_
#define SHAPEFILLWIDGET_H_
#include <QWidget>
class QHBoxLayout;
class QVBoxLayout;
class QPushButton;
class QLineEdit;
class QLabel;
class ColorDialog;
/// <summary>
/// 图形的填充界面
/// </summary>
class ShapeFillWidget:public QWidget
{
	Q_OBJECT
public:
	ShapeFillWidget(QWidget* parent = nullptr);
	~ShapeFillWidget();
public slots:
	void setShapefillParameter(const QColor&color,const int&trans);
signals:
	void sendSelectColor(const QColor& color);
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initSignals();
	void updateLineValue(const QColor& color);


private:
	QPushButton* m_colorBtn = nullptr;
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QLabel* m_functionName = nullptr;
	QLineEdit* m_colorEdt = nullptr;
	QLineEdit* m_hueEdt = nullptr;
	ColorDialog* m_colorDialog = nullptr;
	QColor m_color;

};



#endif
