﻿#include "components/controlclass/createsvgbutton/createsvgbutton.h"
#include <QHBoxLayout>
#include <QPainter>
#include <QColor>
#include <QPaintEvent>
#include <QEvent>
#include <QMouseEvent>
#include <QPoint>
#include <QStyleOption>
#include <QFont>
#include <QDebug>
#pragma execution_character_set("utf-8")
CreateSvgButton::CreateSvgButton(const QString& buttonFunName,QWidget* parent /*=nullptr*/):QPushButton(parent),
	m_buttonName(buttonFunName),
	m_color(QColor("#FFFFFF"))
{
	initWidget();
	initBtn();
	m_whiteBackgroundBtn->installEventFilter(this);
	m_greyBackgroundBtn->installEventFilter(this);
	m_blackBackgroundBtn->installEventFilter(this);
	initSignals();
}

CreateSvgButton::~CreateSvgButton()
{

}

void CreateSvgButton::paintEvent(QPaintEvent* event)
{
	QPainter p(this);
	p.setRenderHints(QPainter::Antialiasing, true);
	QPen pen;
	pen.setWidth(2);
	pen.setColor("#8A2BE2");
	QBrush brush;
	brush.setColor(m_color);
	brush.setStyle(Qt::SolidPattern);
	p.setPen(pen);
	p.setBrush(brush);
	p.drawRoundedRect(QRect(1, 1, this->width() - 2, this->height() - 2), 30, 30);
	QStyleOption opt;
	opt.init(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	drawCross(&p);
	QFont font("Simsun", 12, 10);
	p.setFont(font);
	p.drawText(57, 140,m_buttonName);
	p.end();
}

void CreateSvgButton::enterEvent(QEvent* event)
{
	this->setCursor(Qt::PointingHandCursor);
	m_color = QColor("#C1FFC1");
	update();
	QPushButton::enterEvent(event);
}

void CreateSvgButton::leaveEvent(QEvent* event)
{
	m_color = QColor("#FFFFFF");
	update();
	QPushButton::enterEvent(event);
}

bool CreateSvgButton::eventFilter(QObject* watched, QEvent* event)
{
	if (event->type() == QMouseEvent::HoverEnter) {
		if (watched == m_whiteBackgroundBtn) {
			setBackgroundColor(QColor("#FFFFFF"));
		}
		else if (watched == m_greyBackgroundBtn)
		{
			setBackgroundColor(QColor("#E6E6E6"));

		}
		else if (watched == m_blackBackgroundBtn)
		{
			setBackgroundColor(QColor("#333333"));

		}

	}
	return QPushButton::eventFilter(watched, event);
}

void CreateSvgButton::initWidget()
{
	QHBoxLayout* m_widgethb = new QHBoxLayout;
	m_bottomwidget = new QWidget(this);
	m_whiteBackgroundBtn = new QPushButton(m_bottomwidget);
	m_whiteBackgroundBtn->setObjectName("whiteBackgroundBtn");
	m_greyBackgroundBtn = new QPushButton(m_bottomwidget);
	m_greyBackgroundBtn->setObjectName("greyBackgroundBtn");
	m_blackBackgroundBtn = new QPushButton(m_bottomwidget);
	m_blackBackgroundBtn->setObjectName("blackBackgroundBtn");
	m_widgethb->addStretch();
	m_widgethb->addWidget(m_whiteBackgroundBtn);
	m_widgethb->addWidget(m_greyBackgroundBtn);
	m_widgethb->addWidget(m_blackBackgroundBtn);
	m_widgethb->addStretch();
	m_bottomwidget->setLayout(m_widgethb);
	
}

void CreateSvgButton::initSignals()
{
	connect(this, &QPushButton::clicked, this, [=]() {
		emit sendCreateNewWorkStation();
		});
	connect(m_greyBackgroundBtn, &QPushButton::clicked, this, [=]() {
		emit sendCreateNewWorkStation();
		});
	connect(m_whiteBackgroundBtn, &QPushButton::clicked, this, [=]() {
		emit sendCreateNewWorkStation();
		});
	connect(m_blackBackgroundBtn, &QPushButton::clicked, this, [=]() {
		emit sendCreateNewWorkStation();
		});
}

void CreateSvgButton::drawCross(QPainter* painter)
{
	painter->save();
	QPen pen;
	pen.setColor("#FFD700");
	pen.setWidth(5);
	painter->setPen(pen);
	QPoint s1(this->width() / 2, 25);
	QPoint e1(this->width() / 2, 105);
	QPoint s2(this->width() / 2 - 40, 65);
	QPoint e2(this->width() / 2 + 40, 65);
	painter->drawLine(s2, e2);
	painter->drawLine(s1, e1);
	painter->restore();
}

void CreateSvgButton::initBtn()
{
	m_vb = new QVBoxLayout;
	m_vb->addStretch();
	m_vb->addWidget(m_bottomwidget);
	this->setLayout(m_vb);
}

void CreateSvgButton::setBackgroundColor(QColor color)
{
	m_color = color;
	update();
}

