﻿#ifndef CREATESVGBUTTON_H_
#define CREATESVGBUTTON_H_
#include <QPushButton>
#include <QColor>
#include <QString>
/// <summary>
/// 新建文件的按钮
/// </summary>
class QPushButton;
class QHBoxLayout;
class QVBoxLayout;
class QPainter;
class CreateSvgButton: public QPushButton	
{
	Q_OBJECT
public:
	CreateSvgButton(const QString& buttonFunName,QWidget* parent =nullptr);
	~CreateSvgButton();
protected:
	void paintEvent(QPaintEvent* event) override;
	void enterEvent(QEvent* event) override;
	void leaveEvent(QEvent* event) override;
	bool eventFilter(QObject* watched, QEvent* event) override;
private:
	void initWidget();
	void initSignals();
	void drawCross(QPainter* painter);
	void initBtn();
	void setBackgroundColor(QColor color);
signals:
	/// <summary>
/// 创建新工作区的信号
/// 	1:white
/// 	2:gray
/// 	3:black
/// </summary>
/// 
	void sendCreateNewWorkStation();
private:
	QPushButton* m_deafaultBackgroundBtn = nullptr;
	QPushButton* m_blackBackgroundBtn = nullptr;
	QPushButton* m_whiteBackgroundBtn = nullptr;
	QPushButton* m_greyBackgroundBtn = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QHBoxLayout* m_hb = nullptr;
	QColor m_color;
	QWidget* m_bottomwidget = nullptr;
	QString m_buttonName;
};
#endif
