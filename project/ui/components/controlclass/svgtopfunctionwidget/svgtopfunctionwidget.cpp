﻿#include "components/controlclass/svgtopfunctionwidget/svgtopfunctionwidget.h"
#include <QStyleOption>
#include <QPainter>
#include <QButtonGroup>
#include <QHBoxLayout>
#include <QPushButton>
#include <QEvent>
#include <QMouseEvent>
#include <QDebug>
#pragma execution_character_set("utf-8")
SvgTopFunctionWidget::SvgTopFunctionWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
	,isOpenBorder(false)
	,isOpenEdit(false)
	,isOPenGrid(false)
	,isOpenRuler(false)
{
	initWidget();
	initSignals();
	//m_showgridBtn->installEventFilter(this);
	//m_showrulerBtn->installEventFilter(this);
	//m_borderBtn->installEventFilter(this);
	//m_editBtn->installEventFilter(this);
}

SvgTopFunctionWidget::~SvgTopFunctionWidget()
{

}

void SvgTopFunctionWidget::initSignals()
{
	connect(m_showgridBtn, &QPushButton::clicked, this, [=]() {
		if (isOPenGrid == false) {
			isOPenGrid = true;
			emit sendOpGrid(true);
		}
		else
		{
			isOPenGrid = false;
			emit sendOpGrid(false);
		}
		});
	connect(m_showrulerBtn, &QPushButton::clicked, this, [=]() {
		
		if (isOpenRuler == false) {
			isOpenRuler = true;
			emit sendOpenRuler(true);
		}
		else
		{
			isOpenRuler = false;
			emit sendOpenRuler(false);
		}
		});
	connect(m_borderBtn, &QPushButton::clicked, this, [=]() {
		if (isOpenBorder == false) {
			isOpenBorder = true;
			emit sendOpenBorder(true);
		}
		else
		{
			isOpenBorder = false;
			emit sendOpenBorder(false);
		}
		});
	connect(m_editBtn, &QPushButton::clicked, this, [=]() {
		if (isOpenEdit == false) {
			isOpenEdit = true;
			emit sendOPenEdit(true);
		}
		else
		{
			isOpenEdit = false;
			emit sendOPenEdit(false);
		}
		});
}

void SvgTopFunctionWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

bool SvgTopFunctionWidget::eventFilter(QObject* watched, QEvent* event)
{
	/*if (watched == m_showgridBtn || watched == m_showrulerBtn || watched == m_borderBtn || watched == m_editBtn) {
		if (event->type() == QEvent::HoverEnter) {
			this->setCursor(Qt::PointingHandCursor);
			return true;
		}
	}*/
	if (event->type() == QMouseEvent::HoverEnter) 
	{
		if (watched == m_showgridBtn || watched == m_showrulerBtn || watched == m_borderBtn || watched == m_editBtn) 
		{
			this->setCursor(Qt::PointingHandCursor);
			return true;
		}
	}
	return QWidget::eventFilter(watched, event);
}

void SvgTopFunctionWidget::initWidget()
{
	m_btngroup = new QButtonGroup;
	m_hb = new QHBoxLayout;
	m_worktaskBtn = new QPushButton(this);
	m_worktaskBtn->setToolTip("菜单");
	m_worktaskBtn->setFixedSize(50,50);
	m_showrulerBtn = new QPushButton(this);
	m_showrulerBtn->setToolTip("显示标尺");
	m_showrulerBtn->setFixedSize(40, 40);
	m_borderBtn = new QPushButton(this);
	m_borderBtn->setFixedSize(40, 40);
	m_borderBtn->setToolTip("显示边距");
	m_editBtn = new QPushButton(this);
	m_editBtn->setFixedSize(40, 40);
	m_editBtn->setToolTip("编辑源文件");

	m_showgridBtn = new QPushButton(this);
	m_showgridBtn->setFixedSize(40, 40);
	m_showgridBtn->setToolTip("显示网格");
	QPushButton* m_line = new QPushButton(this);
	//m_line->setFixedSize(1,50);
	m_line->setFixedWidth(1);
	m_saveBtn = new QPushButton(this);
	m_saveBtn->setFixedSize(40, 40);
	m_saveBtn->setToolTip("保存文件");
	m_hb->addWidget(m_worktaskBtn);
	m_hb->addStretch();
	m_hb->addWidget(m_showgridBtn);
	m_hb->addWidget(m_showrulerBtn);
	m_hb->addWidget(m_borderBtn);
	m_hb->addWidget(m_line);
	m_hb->addWidget(m_editBtn);
	m_hb->addStretch();
	m_hb->addWidget(m_saveBtn);

	//m_btngroup->addButton(m_worktaskBtn);
	m_worktaskBtn->setCheckable(true);
	//m_btngroup->addButton(m_showgridBtn);
	m_showgridBtn->setCheckable(true);
	//m_btngroup->addButton(m_showrulerBtn);
	m_showrulerBtn->setCheckable(true);
	//m_btngroup->addButton(m_borderBtn);
	m_borderBtn->setCheckable(true);
	//m_btngroup->addButton(m_editBtn);
	m_editBtn->setCheckable(true);
	//m_btngroup->setExclusive(true);


	m_worktaskBtn->setObjectName("svgworkBtn");
	m_showgridBtn->setObjectName("svggridBtn");
	m_showrulerBtn->setObjectName("svgrulerBtn");
	m_borderBtn->setObjectName("svgborderBtn");
	m_editBtn->setObjectName("svgeditBtn");
	m_saveBtn->setObjectName("svgsaveBtn");

	m_worktaskBtn->setProperty("svgtopbar", "true");
	m_showrulerBtn->setProperty("svgtopbar", "true");
	m_showgridBtn->setProperty("svgtopbar", "true");
	m_editBtn->setProperty("svgtopbar", "true");
	m_borderBtn->setProperty("svgtopbar", "true");
	m_saveBtn->setProperty("svgtopbar", "true");
	m_hb->setContentsMargins(0, 0, 0, 0);
	this->setLayout(m_hb);


	
}

