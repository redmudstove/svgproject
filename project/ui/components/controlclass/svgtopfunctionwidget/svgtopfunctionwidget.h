#ifndef SVGTOPFUNCTIONWIDGET_H_
#define SVGTOPFUNCTIONWIDGET_H_
#include <QWidget>
class QPushButton;
class QHBoxLayout;
class QVBoxLayout;
class QButtonGroup;
class SvgTopFunctionWidget:public QWidget
{
	Q_OBJECT
public:
	SvgTopFunctionWidget(QWidget* parent = nullptr);
	~SvgTopFunctionWidget();
protected:
	void initSignals();
	void paintEvent(QPaintEvent* event) override;
	bool eventFilter(QObject* watched, QEvent* event) override;
private:
	void initWidget();

signals:
	void sendOpGrid(bool flags);
	void sendOpenRuler(bool flags);
	void sendOPenEdit(bool flags);
	void sendOpenBorder(bool flags);
private:
	QHBoxLayout* m_hb = nullptr;
	QPushButton* m_worktaskBtn = nullptr;
	QPushButton* m_showrulerBtn = nullptr;
	QPushButton* m_showgridBtn = nullptr;
	QPushButton* m_borderBtn = nullptr;
	QPushButton* m_editBtn = nullptr;
	QPushButton* m_saveBtn = nullptr;
	QButtonGroup* m_btngroup = nullptr;

private:
	bool isOPenGrid;
	bool isOpenRuler;
	bool isOpenEdit;
	bool isOpenBorder;

};





#endif
