﻿#include <QPainter>
#include <QMouseEvent>
#include <QtMath>
#include "colorslidshowwidget.h"
#include <QDebug>
ColorSlidShowWidget::ColorSlidShowWidget(QWidget *parent)
	: QWidget(parent)
	, m_iHue(0)
	, m_iSaturation(0)
	, m_iBrightness(0)
	, m_iAreaWidth(256)
{
	//创建色彩饱和度的
	createSVPixmap();
	//创建亮度的
	createVPixmap();
	updateSVPixmap();
}

ColorSlidShowWidget::~ColorSlidShowWidget()
{

}

void ColorSlidShowWidget::setHue(int h)
{
	m_iHue = h;
	updateSVPixmap();
	update();
}

void ColorSlidShowWidget::setSaturation(int s)
{
	m_iSaturation = s;
	update();
}

void ColorSlidShowWidget::setBrightness(int v)
{
	m_iBrightness = v;
	update();
}

void ColorSlidShowWidget::setHsv(int h, int s, int v)
{
	m_iHue = h;
	m_iSaturation = s;
	m_iBrightness = v;
	updateSVPixmap();
	update();
}

void ColorSlidShowWidget::setColor(const QColor &c)
{
	m_iHue = c.hue();
	m_iSaturation = c.saturation();
	m_iBrightness = c.value();
	updateSVPixmap();
	emit svChangedSignal(m_iHue, m_iSaturation, m_iBrightness);
	update();
}

void ColorSlidShowWidget::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.drawPixmap(0, 0, m_svPixmap);
	painter.drawPixmap(0, 0, m_vPixmap);
	painter.setPen(QPen(m_iBrightness > 128 ? Qt::black : Qt::white, 2));
	QPointF selecPos = QPointF(m_iSaturation, 255-m_iBrightness);
	painter.drawEllipse(selecPos, 6, 6);
}

void ColorSlidShowWidget::mousePressEvent(QMouseEvent *ev)
{
	m_iSaturation = qBound(0, ev->x(), 255);
	m_iBrightness = qBound(0, 255 - ev->y(), 255);
	emit svChangedSignal(m_iHue, m_iSaturation, m_iBrightness);
	update();
}

void ColorSlidShowWidget::mouseMoveEvent(QMouseEvent *ev)
{
	m_iSaturation = qBound(0, ev->x(), 255);
	m_iBrightness = qBound(0, 255 - ev->y(), 255);
	emit svChangedSignal(m_iHue, m_iSaturation, m_iBrightness);
	update();
}

void ColorSlidShowWidget::createVPixmap()
{
	m_vPixmap = QPixmap(m_iAreaWidth, m_iAreaWidth);
	m_vPixmap.fill(Qt::transparent);
	//在亮度的pixmap上绘制
	QPainter painter(&m_vPixmap);
	painter.setRenderHint(QPainter::Antialiasing);
	//图片叠加模式
	painter.setCompositionMode(QPainter::CompositionMode_Source);
	QLinearGradient vGradient(0, 0, 0, m_iAreaWidth);
	vGradient.setColorAt(0, QColor(0, 0, 0, 0));
	vGradient.setColorAt(1, QColor(0, 0, 0, 255));
	painter.setPen(Qt::NoPen);
	painter.setBrush(QBrush(vGradient));
	painter.drawRect(0, 0, m_iAreaWidth, m_iAreaWidth);
}

void ColorSlidShowWidget::createSVPixmap()
{
	m_svPixmap = QPixmap(m_iAreaWidth, m_iAreaWidth);
	m_svPixmap.fill(Qt::transparent);
}

void ColorSlidShowWidget::updateSVPixmap()
{
	QColor newColor;
	newColor.setHsv(m_iHue, 255, 255);
	QPainter painter(&m_svPixmap);
	painter.setRenderHint(QPainter::Antialiasing);
	QLinearGradient svGradient(0, 0, m_iAreaWidth, 0);
	svGradient.setColorAt(1, newColor);
	svGradient.setColorAt(0, QColor("#ffffff"));
	painter.setPen(Qt::NoPen);
	painter.setBrush(QBrush(svGradient));
	painter.drawRect(0, 0, m_iAreaWidth, m_iAreaWidth);
}

void ColorSlidShowWidget::hueChangedSlot(int h)
{
	m_iHue = h;
	updateSVPixmap();
	emit svChangedSignal(m_iHue, m_iSaturation, m_iBrightness);
	update();
}