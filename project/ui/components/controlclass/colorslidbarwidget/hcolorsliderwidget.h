﻿#ifndef HCOLORSLIDERWIDGET_H_
#define HCOLORSLIDERWIDGET_H_
#include <QWidget>
/// <summary>
/// 竖直 的颜色滑动选择器
/// </summary>
class HColorSliderWidget:public QWidget	
{
	Q_OBJECT
public:
	HColorSliderWidget(QWidget*parent = nullptr);
	~HColorSliderWidget();
	void setHue(int);
protected:
	void paintEvent(QPaintEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
signals:
	void hueChangedSignal(int);
private:
	void createHuePixmap();

private:
	QPixmap m_huePixmap;
	double m_hue;
	int m_iHue;
	const int m_iColorHeight;
	const int m_iColorwidth;
	const int topMargin = 7;
	const int rightMargin = 9;
};

#endif