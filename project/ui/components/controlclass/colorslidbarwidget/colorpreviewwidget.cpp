#include <QStylePainter>
#include "ui/components/controlclass/colorslidbarwidget/colorpreviewwidget.h"
#include <QDebug>
ColorPrewViewWidget::ColorPrewViewWidget(QWidget *parent)
	: QWidget(parent)
	, m_curColor(QColor("#a597be"))
	, m_newColor(QColor("#a597be"))
{

}

ColorPrewViewWidget::~ColorPrewViewWidget()
{

}

void ColorPrewViewWidget::setCurColor(const QColor &c)
{
	m_curColor = c;
	repaint();
}

void ColorPrewViewWidget::setNewColor(const QColor &c)
{
	m_newColor = c;
	update();
}

void ColorPrewViewWidget::paintEvent(QPaintEvent *)
{
	QStylePainter painter(this);
	paint(painter, geometry());
}

void ColorPrewViewWidget::resizeEvent(QResizeEvent *)
{
	update();
}

void ColorPrewViewWidget::paint(QPainter &painter, QRect rect) const
{
	int iMiddleWidth = rect.width() / 2;
	int iHeight = rect.height();
	painter.fillRect(0, 0, iMiddleWidth, iHeight, m_curColor);
	painter.fillRect(iMiddleWidth, 0, iMiddleWidth, iHeight, m_newColor);
	painter.setPen(QPen(Qt::black, 1));
	painter.drawRect(0, 0, width() - 1, height() - 1);
}

void ColorPrewViewWidget::svChangedSlot(int h, int s, int v)
{
	m_newColor.setHsv(h, s, v);
	update();
	emit svChangedSignal(h, s, v);
}
