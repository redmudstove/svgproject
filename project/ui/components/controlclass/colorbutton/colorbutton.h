﻿#ifndef COLORBUTTON_H_
#define COLORBUTTON_H_
#include <QPushButton>

/// <summary>
/// 颜色按钮。
/// </summary>
class ColorButton :public QPushButton 
{
	Q_OBJECT
public:
	ColorButton(QColor color = Qt::white,QWidget*parent = nullptr);
	~ColorButton();

	void setColor(const QColor color);
	QColor getColor() const;

signals:
	void sendColor(QColor color) const;
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initSiganls();
	void initWidget();
	void setBackgroundColor();

private:
	QColor m_saveColor;



};



#endif // !COLORBUTTON_H_
