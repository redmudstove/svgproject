#include "components/controlclass/colorbutton/colorbutton.h"
#include <QPainter>
#include <QColor>
#include <QPaintEvent>
//#include <QBrush>
#include <QStyleOption>
ColorButton::ColorButton(QColor color,QWidget* parent /*= nullptr*/):
	m_saveColor(color),
	QPushButton(parent)
{
	setBackgroundColor();
	initSiganls();
}

ColorButton::~ColorButton()
{

}

void ColorButton::setColor(QColor color)
{
	m_saveColor = color;
}

QColor ColorButton::getColor() const
{
	return m_saveColor;
}

void ColorButton::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QPushButton::paintEvent(event);
}



void ColorButton::initSiganls()
{
	connect(this, &QPushButton::clicked, [=]() {
		//setBackgroundColor();

		emit sendColor(m_saveColor);
		//update();
		});
}

void ColorButton::initWidget()
{

}

void ColorButton::setBackgroundColor()
{
	QPalette pal;
	pal.setColor(QPalette::Button, m_saveColor);
	this->setPalette(pal);
	this->setAutoFillBackground(true);
	this->setFlat(true);
}
