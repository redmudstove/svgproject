﻿#ifndef POLOYGONBUTTON_H_
#define POLOYGONBUTTON_H_
#include "components/controlclass/ploygonbutton/polygonselectwidget/svgpolygonselectwidget.h"
#include <QPushButton>
#include <QPolygon>
//class QPushButton;
class QGridLayout;
class QHBoxLayout;
class QVBoxLayout;
/// <summary>
/// 多边形的组合按按钮。
/// </summary>
class SvgPoloygonButton: public QPushButton
{
	Q_OBJECT
public:
	SvgPoloygonButton(QWidget*parent = nullptr);
	~SvgPoloygonButton();
protected:
	void paintEvent(QPaintEvent* event) override;
	bool eventFilter(QObject* watched, QEvent* event) override;
private:
	void initWidget();
	void initSignals();
private:
	QPushButton* m_selectShapeBtn = nullptr;
	QPushButton* m_triselectBtn = nullptr;
	QGridLayout* m_grid = nullptr;
	QVBoxLayout* m_vb =nullptr;
	QHBoxLayout* m_hb = nullptr;
	SvgPolygonSelectWidget* m_svgpologonwidget = nullptr;
	QPolygon m_polygon;
};


#endif
