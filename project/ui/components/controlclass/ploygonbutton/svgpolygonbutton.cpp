#include "components/controlclass/ploygonbutton/svgpolygonbutton.h"

#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QStyleOption>
#include <QPainter>
#include <QColor>
#include <QBrush>
#include <QPen>
#include <qevent.h>
SvgPoloygonButton::SvgPoloygonButton(QWidget* parent /*= nullptr*/):QPushButton(parent)
{
	this->setFixedSize(44, 44);
	m_svgpologonwidget = new SvgPolygonSelectWidget(this);
	m_svgpologonwidget->setFixedWidth(54);
	m_svgpologonwidget->hide();
	//m_svgpologonwidget->setFixedSize(50, 180);
	m_svgpologonwidget->setWindowFlag(Qt::Popup);
	initWidget();
	initSignals();
	installEventFilter(this);
}

SvgPoloygonButton::~SvgPoloygonButton()
{

}

void SvgPoloygonButton::paintEvent(QPaintEvent* event)
{
	QPainter p(this);
	/*p.save();
	QBrush brush(Qt::SolidPattern);
	brush.setColor(Qt::red);
	p.setBrush(brush);
	p.drawPolygon(m_polygon);
	p.restore();*/

	p.save();
	QStyleOption opt;
	opt.init(this);

	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QPushButton::paintEvent(event);
	p.restore();
}

bool SvgPoloygonButton::eventFilter(QObject* watched, QEvent* event)
{
	if (event->type() == QMouseEvent::HoverEnter || event->type() == QMouseEvent::HoverLeave || event->type() == QMouseEvent::HoverMove) {
		if (watched == m_selectShapeBtn) {
			return QPushButton::eventFilter(watched, event);
		}
	}
	return QPushButton::eventFilter(watched, event);
}

void SvgPoloygonButton::initWidget()
{
	m_hb = new QHBoxLayout;
	m_triselectBtn = new QPushButton(this);
	m_triselectBtn->setFixedSize(12, 12);
	m_hb->addStretch();
	m_hb->addWidget(m_triselectBtn);
	m_hb->setContentsMargins(0, 0, 0, 0);

	m_vb = new QVBoxLayout;
	m_vb->addStretch();
	m_vb->addLayout(m_hb);
	m_vb->setContentsMargins(0, 0, 0, 0);
	this->setIconSize(QSize(32, 32));
	this->setIcon(QIcon(":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/starnormal.png"));
	//m_selectShapeBtn->setObjectName("svgselectshapeBtn");
	m_triselectBtn->setObjectName("svgselecttriBtn");
	this->setLayout(m_vb);
	
}

void SvgPoloygonButton::initSignals()
{
	connect(m_triselectBtn, &QPushButton::clicked, this, [=]() {
		m_svgpologonwidget->move(QCursor::pos().x()+10, QCursor::pos().y() -130);
		m_svgpologonwidget->show();
		});
	connect(m_svgpologonwidget, &SvgPolygonSelectWidget::sendSelectShape, [=](QString path) {
		this->setIcon(QIcon(path));
		});
}


