﻿#ifndef SVGPOLYGONSELECTWIDGET_H_
#define SVGPOLYGONSELECTWIDGET_H_
#include <QWidget>
#include <QVector>
class QPushButton;
class QVBoxLayout;
#include <QString>
/// <summary>
/// 放置指定的多边形。
/// </summary>
const QVector<QString> Pathnormal = {":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/starnormal.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/trianglenormal.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/rightnormal.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/diamondnormal.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/polygonnormal.png"};
const QVector<QString> Pathpressed = {":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/starpressed.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/trianglepressed.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/rightpressed.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/diamondpressed.png",":/drawtoolswidget/res/icons/ui/control/shapeselectbutton/polygonpressed.png"};
class SvgPolygonSelectWidget:public QWidget
{
	Q_OBJECT
public:
	SvgPolygonSelectWidget(QWidget*parent = nullptr);
	~SvgPolygonSelectWidget();
protected:
	void paintEvent(QPaintEvent* event) override;
	void leaveEvent(QEvent* event) override;
signals:
	void sendSelectShape(QString path);
private:
	void initwidget();
	void initSignals();
private:
	QPushButton* m_starBtn = nullptr;
	QPushButton* m_lsosTriBtn = nullptr;
	QPushButton* m_rightTriBtn = nullptr;
	QPushButton* m_diamondBtn = nullptr;
	QPushButton* m_polygonBtn = nullptr;
	QVBoxLayout* m_vb = nullptr;

};

#endif
