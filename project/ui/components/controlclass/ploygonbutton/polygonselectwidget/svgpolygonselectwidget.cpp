#include "components/controlclass/ploygonbutton/polygonselectwidget/svgpolygonselectwidget.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QStyleOption>
#include <QPainter>
SvgPolygonSelectWidget::SvgPolygonSelectWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initwidget();
	initSignals();
}

SvgPolygonSelectWidget::~SvgPolygonSelectWidget()
{

}

void SvgPolygonSelectWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}


void SvgPolygonSelectWidget::leaveEvent(QEvent* event)
{
	this->hide();
}

void SvgPolygonSelectWidget::initwidget()
{
	m_vb = new QVBoxLayout;
	m_starBtn = new QPushButton(this);
	m_starBtn->setFixedSize(48, 48);
	m_starBtn->setIconSize(QSize(32,32));
	m_starBtn->setIcon(QIcon(Pathnormal[0]));
	m_lsosTriBtn = new QPushButton(this);
	m_lsosTriBtn->setIconSize(QSize(32,32));
	m_lsosTriBtn->setIcon(QIcon(Pathnormal[1]));
	m_lsosTriBtn->setFixedSize(48, 48);
	m_rightTriBtn = new QPushButton(this);
	m_rightTriBtn->setIconSize(QSize(32, 32));
	m_rightTriBtn->setIcon(QIcon(Pathnormal[2]));
	m_rightTriBtn->setFixedSize(48, 48);
	m_diamondBtn = new QPushButton(this);
	m_diamondBtn->setIconSize(QSize(32, 32));
	m_diamondBtn->setIcon(QIcon(Pathnormal[3]));
	m_diamondBtn->setFixedSize(48, 48);
	m_polygonBtn = new QPushButton(this);
	m_polygonBtn->setIconSize(QSize(32, 32));
	m_polygonBtn->setIcon(QIcon(Pathnormal[4]));
	m_polygonBtn->setFixedSize(48, 48);

	m_starBtn->setProperty("selectshapeBtn", "true");
	m_lsosTriBtn->setProperty("selectshapeBtn", "true");
	m_rightTriBtn->setProperty("selectshapeBtn", "true");
	m_diamondBtn->setProperty("selectshapeBtn", "true");
	m_polygonBtn->setProperty("selectshapeBtn", "true");

	m_vb->addWidget(m_starBtn);
	m_vb->addWidget(m_lsosTriBtn);
	m_vb->addWidget(m_rightTriBtn);
	m_vb->addWidget(m_diamondBtn);
	m_vb->addWidget(m_polygonBtn);
	m_vb->setSpacing(0);
	m_vb->setContentsMargins(2, 5, 0, 2);
	this->setLayout(m_vb);

}

void SvgPolygonSelectWidget::initSignals()
{
	connect(m_starBtn, &QPushButton::clicked, this, [=]() {
		emit sendSelectShape(Pathpressed[0]);
		});
	connect(m_lsosTriBtn, &QPushButton::clicked, this, [=]() {
		emit sendSelectShape(Pathpressed[1]);
		});
	connect(m_rightTriBtn, &QPushButton::clicked, this, [=]() {
		emit sendSelectShape(Pathpressed[2]);
		});
	connect(m_diamondBtn, &QPushButton::clicked, this, [=]() {
		emit sendSelectShape(Pathpressed[3]);
		});
	connect(m_polygonBtn, &QPushButton::clicked, this, [=]() {
		emit sendSelectShape(Pathpressed[4]);
		});
}

