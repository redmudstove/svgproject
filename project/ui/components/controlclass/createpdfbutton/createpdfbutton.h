#ifndef CREATEPDFBUTTON_H_
#define CREATEPDFBUTTON_H_
#include <QPushButton>
class CreatePdfButton:public QPushButton
{
	Q_OBJECT
public:
	CreatePdfButton();
	~CreatePdfButton();
protected:
	void paintEvent(QPaintEvent* event);

private:
};


#endif