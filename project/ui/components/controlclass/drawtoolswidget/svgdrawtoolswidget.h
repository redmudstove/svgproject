﻿#ifndef SVGDRAWTOOLSWIDGET_H_
#define SVGDRAWTOOLSWIDGET_H_
#include <QWidget>
#include "kernel/shape/svgabstractshape.h"
class QVBoxLayout;
class QPushButton;
class SvgPoloygonButton;
class QButtonGroup;
/// <summary>
/// 图形栏
/// </summary>
class  SvgDrawToolsWidget:public QWidget
{
	Q_OBJECT
public:
	SvgDrawToolsWidget(QWidget* parent = nullptr);
	~SvgDrawToolsWidget();
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initSignals();
	
signals:
	void setCurOPreation(const Shape::ShapeType shapetype);
private:
	QPushButton* m_selectBtn = nullptr;
	QPushButton* m_lineBtn = nullptr;
	QPushButton* m_freellineBtn = nullptr;
	QPushButton* m_rectBtn = nullptr;
	QPushButton* m_circleBtn = nullptr;
	SvgPoloygonButton* m_polygonBtn = nullptr;
	QPushButton* m_penBtn = nullptr;
	QPushButton* m_textBtn = nullptr;
	QPushButton* m_eyedroperBtn = nullptr;
	QPushButton* m_shapezoomBtn = nullptr;
	QVBoxLayout* m_vb = nullptr;
	QButtonGroup* m_group = nullptr;
};



#endif // !SVGDRAWTOOLSWIDGET_H_
