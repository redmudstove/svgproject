#include "components/controlclass/drawtoolswidget/svgdrawtoolswidget.h"
#include "components/controlclass/ploygonbutton/svgpolygonbutton.h"
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QVBoxLayout>
#include <QPainter>
#include <QStyleOption>
#include <QPushButton>
#include <QDebug>
SvgDrawToolsWidget::SvgDrawToolsWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

SvgDrawToolsWidget::~SvgDrawToolsWidget()
{

}

void SvgDrawToolsWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}

void SvgDrawToolsWidget::initWidget()
{
	m_vb = new QVBoxLayout;
	m_group = new QButtonGroup;

	m_selectBtn = new QPushButton(this);
	m_lineBtn = new QPushButton(this);
	m_freellineBtn = new QPushButton(this);
	m_rectBtn = new QPushButton(this);
	m_circleBtn = new QPushButton(this);
	m_polygonBtn = new SvgPoloygonButton(this);
	m_textBtn = new QPushButton(this);
	m_eyedroperBtn = new QPushButton(this);
	m_shapezoomBtn = new QPushButton(this);
	m_penBtn = new QPushButton(this);

	m_selectBtn->setFixedSize(48, 48);
	m_lineBtn->setFixedSize(48, 48);
	m_freellineBtn->setFixedSize(48, 48);
	m_rectBtn->setFixedSize(48, 48);
	m_circleBtn->setFixedSize(48, 48);
	m_textBtn->setFixedSize(48, 48);
	m_eyedroperBtn->setFixedSize(48, 48);
	m_shapezoomBtn->setFixedSize(48, 48);
	m_penBtn->setFixedSize(48, 48);
	m_polygonBtn->setFixedSize(48, 48);

	m_selectBtn->setCheckable(true);
	m_lineBtn->setCheckable(true);
	m_freellineBtn->setCheckable(true);
	m_rectBtn->setCheckable(true);
	m_circleBtn->setCheckable(true);
	m_textBtn->setCheckable(true);
	m_eyedroperBtn->setCheckable(true);
	m_shapezoomBtn->setCheckable(true);
	m_polygonBtn->setCheckable(true);
	m_penBtn->setCheckable(true);
	
	m_selectBtn->setObjectName("svgselectbtn");
	m_lineBtn->setObjectName("svglinebtn");
	m_freellineBtn->setObjectName("svgfreelinebtn");
	m_rectBtn->setObjectName("svgrectbtn");
	m_circleBtn->setObjectName("svgcirclebtn");
	m_textBtn->setObjectName("svgtextbtn");
	m_eyedroperBtn->setObjectName("svgeyedroperbtn");
	m_shapezoomBtn->setObjectName("svgbackgroundbtn");
	m_polygonBtn->setObjectName("svgpoloybtn");
	m_penBtn->setObjectName("svgpenbtn");

	m_vb->addWidget(m_selectBtn);
	m_selectBtn->setChecked(true);
	m_vb->addWidget(m_lineBtn);
	m_vb->addWidget(m_freellineBtn);
	m_vb->addWidget(m_rectBtn);
	m_vb->addWidget(m_circleBtn);
	m_vb->addWidget(m_polygonBtn);
	m_vb->addWidget(m_penBtn);
	m_vb->addWidget(m_textBtn);
	m_vb->addWidget(m_eyedroperBtn);
	m_vb->addWidget(m_shapezoomBtn);
	m_vb->setContentsMargins(3, 5,3, 5);
	m_vb->addStretch();
	m_vb->setSpacing(5);

	m_group->addButton(m_selectBtn, 0);
	m_group->addButton(m_lineBtn, 1);
	m_group->addButton(m_freellineBtn,2);
	m_group->addButton(m_rectBtn, 3);
	m_group->addButton(m_circleBtn, 4);
	m_group->addButton(m_polygonBtn,5);
	m_group->addButton(m_textBtn, 6);
	m_group->addButton(m_penBtn, 7);
	m_group->addButton(m_eyedroperBtn, 8);
	m_group->addButton(m_shapezoomBtn, 9);
	m_group->setExclusive(true);

	this->setLayout(m_vb);


}

void SvgDrawToolsWidget::initSignals()
{
	connect(m_group, &QButtonGroup::idClicked, [=](int id) {
		switch (id)
		{

		case 0:
			emit setCurOPreation(Shape::ShapeType::select);
			break;
		case 1:
			emit setCurOPreation(Shape::ShapeType::line);
			break;
		case 2:
			emit setCurOPreation(Shape::ShapeType::freeLine);
			break;
		case 3:
			emit setCurOPreation(Shape::ShapeType::rectangle);
			break;
		case 4:
			emit setCurOPreation(Shape::ShapeType::circle);
			
			break;
		case 5:
			emit setCurOPreation(Shape::ShapeType::ellipse);
			
			break;

		case 6:
			emit setCurOPreation(Shape::ShapeType::polyline);
			
			break;
		case 7:
			emit setCurOPreation(Shape::ShapeType::polygon);
		
			break;

		case 8:
			emit setCurOPreation(Shape::ShapeType::path);
	
			break;

		case 9:
			emit setCurOPreation(Shape::ShapeType::shapeZoom);
		
			break;

		case 10:
			emit setCurOPreation(Shape::ShapeType::shapeZoom);
		default:
			break;
		}
		});
}
