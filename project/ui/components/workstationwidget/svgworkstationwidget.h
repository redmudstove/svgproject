﻿#ifndef SVGWORKSTATIONWIDGET_H_
#define SVGWORKSTATIONWIDGET_H_
#include <QWidget>
#include <QGridLayout>
#include "components/controlclass/svgtopfunctionwidget/svgtopfunctionwidget.h"
#include "components/controlclass/drawtoolswidget/svgdrawtoolswidget.h"
#include "components/controlclass/svgtoolswidget/svgtoolswidget.h"
#include "components/controlclass/graphicsprocess/svgdrawarearuler/svgruler.h"
class SvgSketchPad;
class QHBoxLayout;
class QVBoxLayout;
class ColorBarWidget;
class QLabel;
/// <summary>
/// 绘制画布的区域
/// </summary>
class SvgWorkStationWidget:public QWidget	
{
	Q_OBJECT
public:
	SvgWorkStationWidget(int index,QWidget* parent = nullptr);
	~SvgWorkStationWidget();
	int getIndex()const;
	void setIndex(const int& index);
signals:
	void sendOPeationTodo(const Shape::ShapeType shape);
protected:
	void paintEvent(QPaintEvent* event) override;
	//void enterEvent(QEvent* event) override;
	//bool eventFilter(QObject* watched, QEvent* event) override;
private:
	void initWidget();
	void initSignals();
private:
	SvgTopFunctionWidget* m_topfuncwidget = nullptr;
	SvgDrawToolsWidget* m_leftdrawoolswidget = nullptr;
	SvgToolsWidget* m_svgtoolswidget = nullptr;
	ColorBarWidget* m_colorBar = nullptr;
	SvgSketchPad* m_svgsketchPad = nullptr; 
	QGridLayout* m_grid = nullptr;
	QHBoxLayout* m_hb1 = nullptr;
	QHBoxLayout* m_hb2 = nullptr;
	QVBoxLayout* m_vb = nullptr;
	Ruler::SvgHRuler* m_ruler = nullptr;
	QLabel* m_label = nullptr;
	int m_taskIndex;
	//Ruler::SvgVRuler* m_vruler = nullptr;
};
#endif
