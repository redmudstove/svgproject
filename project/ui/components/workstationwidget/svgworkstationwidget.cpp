﻿#include <QHBoxLayout>
#include <QVBoxLayout>
#include "components/workstationwidget/svgworkstationwidget.h"
#include <QStyleOption>
#include <QPainter>
#include <QPaintEvent>
#include <QEvent>
#include "components/controlclass/colorbarwidget/colorbarwidget.h"
#include "components/controlclass/graphicsprocess/svgsketchpad/svgsketchpad.h"
#include <QLabel>
#pragma execution_character_set("utf-8")
SvgWorkStationWidget::SvgWorkStationWidget(int index,QWidget* parent /*= nullptr*/):QWidget(parent),m_taskIndex(index)
{
	initWidget();
	initSignals();
}

SvgWorkStationWidget::~SvgWorkStationWidget()
{

}

int SvgWorkStationWidget::getIndex() const
{
	return m_taskIndex;
}

void SvgWorkStationWidget::setIndex(const int& index)
{
	m_taskIndex = index;
}

void SvgWorkStationWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}



//void SvgWorkStationWidget::enterEvent(QEvent* event)
//{
//	this->setCursor(Qt::PointingHandCursor);
//	QWidget::enterEvent(event);
//}

void SvgWorkStationWidget::initWidget()
{
	//m_grid = new QGridLayout(this);
	m_topfuncwidget = new SvgTopFunctionWidget(this);
	m_leftdrawoolswidget = new SvgDrawToolsWidget(this);
	m_leftdrawoolswidget->setFixedWidth(54);
	//m_leftdrawoolswidget->setMinimumHeight(700);
	m_svgtoolswidget = new SvgToolsWidget(this);
	m_svgtoolswidget->setFixedWidth(300);
	m_colorBar = new ColorBarWidget(this);
	m_svgsketchPad = new SvgSketchPad(this);
	m_svgsketchPad->setMinimumSize(1130,680);
	//m_vruler = new Ruler::SvgVRuler(this);

	//m_vruler = new Ruler::SvgVRuler(this);
	/**/
	//m_colorBar = new ColorBarWidget(this);
	m_ruler = new Ruler::SvgHRuler(this);
	//m_svgcanvas->setFixedSize(500, 500);

	m_vb = new QVBoxLayout(this);
	m_hb1 = new QHBoxLayout;
	m_hb2 = new QHBoxLayout;
	m_label = new QLabel(this);
	m_label->setFixedSize(200, 30);
	m_label->setText("这是第：" + QString::number(m_taskIndex) + " 个工作区");
	m_hb2->addWidget(m_colorBar);
	m_hb2->addWidget(m_label);
	m_hb2->addWidget(m_ruler);

	m_hb2->addStretch();
	m_hb2->setContentsMargins(5, 0, 0, 10);
	m_hb1->setContentsMargins(0, 0, 0, 0);
	m_hb1->addWidget(m_leftdrawoolswidget);
	//m_hb1->addWidget(m_vruler);
	//m_hb1->addStretch();
	m_hb1->addWidget(m_svgsketchPad);
	m_hb1->addWidget(m_svgtoolswidget);



	m_vb->addWidget(m_topfuncwidget);
	m_vb->addLayout(m_hb1);
	
	m_vb->addLayout(m_hb2);
	m_vb->addWidget(m_ruler);
	//m_vb->addStretch();
	m_vb->setSpacing(1);
	m_vb->setContentsMargins(0, 0, 0, 0);
	//m_topfuncwidget->setFixedHeight(50);
	/*m_grid->addWidget(m_topfuncwidget, 0, 0,1,4);
	m_grid->addWidget(m_leftdrawoolswidget, 1, 0, 4, 1);
	m_grid->addWidget(m_svgtoolswidget, 1, 3,4,1);
	m_grid->addWidget(m_colorBar, 5, 0);*/
	//m_grid->addWidget(m_leftdrawoolswidget, 1, 0);
	//m_grid->setContentsMargins(0, 0, 0, 0);
	//this->setLayout(m_grid);
}

void SvgWorkStationWidget::initSignals()
{
	connect(m_topfuncwidget, &SvgTopFunctionWidget::sendOpGrid, m_svgsketchPad, &SvgSketchPad::isOpenGrid);
	connect(m_topfuncwidget, &SvgTopFunctionWidget::sendOpenRuler, m_svgsketchPad, &SvgSketchPad::isOPenRuler);
	connect(m_svgtoolswidget, &SvgToolsWidget::sendCanvasSize, m_svgsketchPad, &SvgSketchPad::setCanvasSize);
	connect(m_leftdrawoolswidget, &SvgDrawToolsWidget::setCurOPreation,m_svgsketchPad,&SvgSketchPad::sendOPeationTodo);
	connect(m_svgsketchPad, &SvgSketchPad::sendCanvasSelect,m_svgtoolswidget,&SvgToolsWidget::setSelectedCanvas);
	//选中图形。
	connect(m_svgsketchPad, &SvgSketchPad::sendShapeSelected, m_svgtoolswidget, &SvgToolsWidget::setSelectedShapes);
	connect(m_svgtoolswidget, &SvgToolsWidget::sendTransvalue, m_svgsketchPad, &SvgSketchPad::setShapesTransvalue);
	connect(m_svgtoolswidget, &SvgToolsWidget::sendSelectColor, m_svgsketchPad, &SvgSketchPad::setShapesColor);
	connect(m_svgtoolswidget, &SvgToolsWidget::sendSelectStrokeColor, m_svgsketchPad, &SvgSketchPad::setShapeStrokeColor);
	//connect(m_svgsketchPad,&SvgSketchPad::sendSlcaing,m_svgtoolswidget)
}

