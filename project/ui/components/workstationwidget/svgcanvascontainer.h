#ifndef SVGCANVASCONTAINER_H_
#define SVGCANVASCONTAINER_H_
#include <QWidget>
#include "kernel/shape/svgabstractshape.h"
#include "ui/components/controlclass/graphicsprocess/svgcanvas/svgcanvas.h"
class QVBoxLayout;
class QHBoxLayout;
class SvgCanvasContainer:public QWidget
{
	Q_OBJECT
public:
	SvgCanvasContainer(QWidget* parent = nullptr);
	~SvgCanvasContainer();

public slots:
	void isOpenGrid(bool flags);
	void setCurShape(const Shape::ShapeType shape);
	void changeSize(const QSize& size);
	void setSelectShapesTrans(const int& value);
	void setSelectShapesfillColor(const QColor& color);
	void setSelectShapsStrokeColor(const QColor& color);
signals:
	void sendCanvasSelect();
	void changeSvgcanvasSize(const QSize&size);
	void sendSelectColor(const QColor&color);
	void sendShapesTransValue(const int&value);
	void sendOpGrid(bool flags);
	void sendOPeationTodo(Shape::ShapeType shape);
	void sendSelectCanvas();
	void sendSelectStrokeColor(const QColor& color);
	void sendSlcaing(const double&sclaing);
	void sendShapeSelected(const int& index, const int& w, const int& h, const int& x, const int& y,
		const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor);
protected:
	void paintEvent(QPaintEvent* event) override;

private:
	void initWidget();
	void initSignals();
private:
	SvgCanvas* m_svgCanvas = nullptr;
	QHBoxLayout* m_hb = nullptr;
	QVBoxLayout* m_vb = nullptr;
};


#endif
