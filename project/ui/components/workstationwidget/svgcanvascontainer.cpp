#include <ui/components/workstationwidget/svgcanvascontainer.h>
#include <QPainter>
#include <QPaintEvent>
#include <QStyleOption>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDebug>
SvgCanvasContainer::SvgCanvasContainer(QWidget* parent /*= nullptr*/) :QWidget(parent)
{
	initWidget();
	initSignals();
}

SvgCanvasContainer::~SvgCanvasContainer()
{

}

void SvgCanvasContainer::isOpenGrid(bool flags)
{
	emit sendOpGrid(flags);
}

void SvgCanvasContainer::setCurShape(const Shape::ShapeType shape)
{
	emit sendOPeationTodo(shape);
}

void SvgCanvasContainer::changeSize(const QSize& size)
{
	emit changeSvgcanvasSize(size);
}

void SvgCanvasContainer::setSelectShapesTrans(const int& value)
{
	emit sendShapesTransValue(value);
}

void SvgCanvasContainer::setSelectShapesfillColor(const QColor& color)
{
	emit sendSelectColor(color);
}

void SvgCanvasContainer::setSelectShapsStrokeColor(const QColor& color)
{
	emit sendSelectStrokeColor(color);
}

void SvgCanvasContainer::paintEvent(QPaintEvent* event)
{

	QStyleOption opt;
	QPainter p(this);
	opt.init(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);

}

void SvgCanvasContainer::initWidget()
{
	m_svgCanvas = new SvgCanvas(this);
	m_hb = new QHBoxLayout(this);
	m_hb->addStretch();
	m_hb->addWidget(m_svgCanvas);
	m_hb->addStretch();
	m_hb->setContentsMargins(0, 0, 0, 0);
	/*m_vb = new QVBoxLayout(this);
	m_vb->addWidget(m_svgCanvas);*/
	
}

void SvgCanvasContainer::initSignals()
{
	connect(this, &SvgCanvasContainer::sendOpGrid, m_svgCanvas, &SvgCanvas::isOpenGrid);
	connect(this, &SvgCanvasContainer::sendOPeationTodo, m_svgCanvas, &SvgCanvas::setCurShape);
	connect(this, &SvgCanvasContainer::changeSvgcanvasSize, m_svgCanvas, &SvgCanvas::changeSize);
	connect(m_svgCanvas, &SvgCanvas::sendSelectCanvas, this, [=]() {
		emit sendCanvasSelect();
		});
	connect(m_svgCanvas, &SvgCanvas::sendSlcaing, this, [=](const double&scaling) {
		emit sendSlcaing(scaling);
		});
	connect(m_svgCanvas, &SvgCanvas::sendShapeSelected, this, [=](const int& index, const int& w, const int& h, const int& x, const int& y,
		const int& ration, const int& trans, const int& blur, const QColor& fillcolor, const QColor& bordercolor) {
			emit sendShapeSelected(index, w, h, x, y, ration, trans, blur, fillcolor, bordercolor);
		});
	connect(this, &SvgCanvasContainer::sendShapesTransValue, m_svgCanvas, &SvgCanvas::setSelectShapesTrans);
	connect(this, &SvgCanvasContainer::sendSelectColor, m_svgCanvas, &SvgCanvas::setSelectShapesfillColor);
	connect(this, &SvgCanvasContainer::sendSelectStrokeColor, m_svgCanvas, &SvgCanvas::setSelectShapsStrokeColor);
}

