#ifndef SELECTSHAPE_H_
#define SELECTSHAPE_H_
#include <QRectF>
#include <QVector>
#include "kernel/shape/svgabstractshape.h"
class QPainter;
class QPen;
class QColor;
const double PIE = 3.1415926;
class SelectRegion
{
public:
	enum class SelectPos
	{
		TopLeft,
		TopMid,
		TopRight,

		MidLeft,
		MidRight,

		BottomLeft,
		BottomMid,
		BottomRight,
		RotatePos,
		UselessPos
	};
public:
	SelectRegion();
	SelectRegion(const QRectF& rect);
	~SelectRegion();

	void setRect(const QRectF& rect, double scaling = 1.0);
	void setRectWidth(const double w);
	void setRotate(const double& rt);
	void setCenterP(const QPointF& centerP);

	bool containsClicked(const QPointF& clickedPos);
	SelectPos graphChangeClicked(const QPointF& clickpos);
	void changeGraph(Shape::SvgAbstractShape* graph, SelectPos& selected, const QPointF& lastp, const QPointF& endp);
	void paintSurroundRect(QPainter* painter);

	QRectF rotateRect(bool judge = true)		const;
	QRectF topLeftRect(bool judge = true)		const;
	QRectF topRightRect(bool judge = true)		const;
	QRectF bottomLeftRect(bool judge = true)	const;
	QRectF bottomRightRect(bool judge = true)	const;
	QRectF topMidRect(bool judge = true)		const;
	QRectF MidLeftRect(bool judge = true)		const;
	QRectF MidRightRect(bool judge = true)		const;
	QRectF bottomMidRect(bool judge = true)		const;
	QRectF ParameterRect(bool judge = true) const;
protected:
private:
	double getAngleInCenter(const QPointF& startp, const QPointF& endp, const QPointF& centerp);
private:
	double m_width;
	double m_rorate;
	double m_scaling;
	QPointF m_centerP;
	QPointF m_topLeft;
	QPointF m_topRight;
	QPointF m_bottomLeft;
	QPointF m_bottomRight;
	QPointF m_topMid;
	QPointF m_midLeft;
	QPointF m_midRight;
	QPointF m_bottomMid;

	int m_rectWidth;
	int m_rectHeight;
};

class SelectRect{
public:
	SelectRect();
	~SelectRect();
	void setPath(const QVector<QPointF>& path);
	void setShapeInSelect(QVector<Shape::SvgAbstractShape*>& shapes);
	bool shapeInSelectRect(Shape::SvgAbstractShape* shape);
	void selectedShapes(QVector<Shape::SvgAbstractShape*>& shapes);
	void moveSelectedShaps(const double& x, const double& y);
	void setSelectedShapsLineColor(const QColor& color);
	void setSelectedGraphsFillColor(const QColor& color);
	void setSelectedGraphsLineType(const Qt::PenStyle& type);
	QVector<Shape::SvgAbstractShape*> copySelectedGraphs();
	void clear();
	QPointF getTopLeft() const;
	int  getGraphsNums() const;
	QVector<Shape::SvgAbstractShape*> getSelectedGraphs()   const;
	bool containsPointF(const QPointF& pointF) const;
	void paintRect(QPainter& painter, QPen& pen, bool visible = false, const double& scaling = 1.0);
	void paintSelectedGraphRect(QPainter& painter, QPen& pen, bool visible = false, const double& scaling = 1.0);
private:
	QPointF m_topLeft;
	QVector<QPointF> m_path;
	QVector<Shape::SvgAbstractShape*> m_shapeSelected;

};
class Grid {

public:
	explicit Grid();
	~Grid();
	void setSize(const int&w,const int& h);
	void paintGrid(QPainter& painter,const double&m_salcing = 1.0);
protected:
private:
	int m_w;
	int m_h;
};
#endif // !1


