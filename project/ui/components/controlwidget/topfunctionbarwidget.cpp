﻿#include "components/controlwidget/topfunctionbarwidget.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QStyleOption>
#include <QPainter>
TopFunctionBarWidget::TopFunctionBarWidget(QWidget* parent /*=nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

TopFunctionBarWidget::~TopFunctionBarWidget()
{

}

void TopFunctionBarWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);

}

void TopFunctionBarWidget::initWidget()
{
	m_vb = new QVBoxLayout;
	//m_vb->addStretch();
	m_creatFileBtn = new QPushButton(this);
	m_creatFileBtn->setObjectName("topFucBarCreateBtn");
	m_creatFileBtn->setIconSize(QSize(32, 32));

	m_creatFileBtn->setIcon(QIcon(":/topfunction/res/icons/ui/widget/topwidget/creatfile.png"));
	m_creatFileBtn->setFixedSize(150, 45);
	m_creatFileBtn->setText("新建文件");
	m_creatFileBtn->setCheckable(true);


	m_openFile = new QPushButton(this);
	m_openFile->setObjectName("topFucBarOPenBtn");
	m_openFile->setIconSize(QSize(32,32));
	m_openFile->setIcon(QIcon(":/topfunction/res/icons/ui/widget/topwidget/checked.png"));
	m_openFile->setFixedSize(150, 45);
	m_openFile->setText("打开文件");
	m_openFile->setCheckable(true);


    m_waifordealBtn = new QPushButton(this);
	m_waifordealBtn->setObjectName("topFucBarWaitBtn");
	m_waifordealBtn->setIconSize(QSize(32, 32));
	m_waifordealBtn->setIcon(QIcon(":/topfunction/res/icons/ui/widget/topwidget/wait.png"));
	m_waifordealBtn->setFixedSize(150, 45);
	m_waifordealBtn->setText("待办事项");
	m_waifordealBtn->setCheckable(true);


	m_recylebinBtn = new QPushButton(this);
	m_recylebinBtn->setObjectName("topFucBarRecleBtn");
	m_recylebinBtn->setFixedSize(150, 45);
	m_recylebinBtn->setIconSize(QSize(32, 32));
	m_recylebinBtn->setIcon(QIcon(":/topfunction/res/icons/ui/widget/topwidget/recyclebin.png"));
	m_recylebinBtn->setText("文件回收");
	m_recylebinBtn->setCheckable(true);

	m_tooboxBtn = new QPushButton(this);
	m_tooboxBtn->setObjectName("topFucBartoolsBtn");
	m_tooboxBtn->setIconSize(QSize(32, 32));
	m_tooboxBtn->setIcon(QIcon(":/topfunction/res/icons/ui/widget/topwidget/toolbox.png"));
	m_tooboxBtn->setFixedSize(150, 45);
	m_tooboxBtn->setText("应用");
	m_tooboxBtn->setCheckable(true);

	m_groupBtn = new QButtonGroup;
	m_groupBtn->addButton(m_creatFileBtn);
	m_groupBtn->addButton(m_openFile);
	m_groupBtn->addButton(m_recylebinBtn);
	m_groupBtn->addButton(m_waifordealBtn);
	m_groupBtn->addButton(m_tooboxBtn);


	m_vb->addWidget(m_creatFileBtn);
	m_vb->addWidget(m_openFile);
	m_vb->addWidget(m_waifordealBtn);
	m_vb->addWidget(m_recylebinBtn);


	m_vb->addStretch();
	m_vb->addWidget(m_tooboxBtn);
	m_vb->setContentsMargins(3, 50, 0, 10);
	this->setLayout(m_vb);


}

void TopFunctionBarWidget::initSignals()
{
	connect(m_creatFileBtn, &QPushButton::clicked, this, [=]() {
		emit sendOpenCreatfilesWidget();
		});
	connect(m_openFile, &QPushButton::clicked, this, [=]() {
		emit sendOpenFilesWidget();
		});
	connect(m_recylebinBtn, &QPushButton::clicked, this, [=]() {
		emit sendRecyleBinWidget();
		});
	connect(m_waifordealBtn, &QPushButton::clicked, this, [=]() {
		emit sendWaitforDealWidget();
		});
}

