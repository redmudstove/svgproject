﻿#include "components/controlwidget/taskbarwidget.h"
#include <QHBoxLayout>
#include <QPushButton>
#include <QPen>
#include <QButtonGroup>
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
#include <QSpacerItem>
int TaskBarWidget::countnum = 0;
TaskBarWidget::TaskBarWidget(QWidget* parent /*= nullptr*/) :QWidget(parent)
{
	initWidget();
	initSignals();
}

TaskBarWidget::~TaskBarWidget()
{
}

void TaskBarWidget::clickedToppage()
{
	emit sendTopPageClicked();
}

void TaskBarWidget::deleteTaskBtn()
{
	TaskButton* btn = qobject_cast<TaskButton*>(sender());
	for (auto it = m_taskBtnList.begin(); it != m_taskBtnList.end()&&(*it)!=nullptr; ++it) 
	{
		if (*it == btn) 
		{
			delete btn;
			m_taskBtnList.erase(it);
			btn=nullptr;
			if (!m_taskBtnList.isEmpty()) {
				auto temp = m_taskBtnList.end() - 1;
				(*temp)->setChecked(true);
			}
			
		}
	}
}

void TaskBarWidget::creatNewTaskBtn()
{

}

void TaskBarWidget::deleteWorkStation(const int& index)
{
	emit sendDeleteWorkSation(index);
}

void TaskBarWidget::setSelectWorkStation(const int& id)
{
	emit sendSelectWorkSation(id);
}

void TaskBarWidget::taskButtonClicked(const int& index)
{
	for (auto it : m_taskBtnList) 
	{
		if (it->getIndex() == index) 
		{
			emit sendCreatNewWorkSation(index);
		}
		else
		{
			it->setChecked(false);
		}
	}
}

void TaskBarWidget::reciviedCreateSignals()
{
	addNewBtn();
}

void TaskBarWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void TaskBarWidget::initWidget()
{
	m_hblayout = new QHBoxLayout;
	m_spacerItem = new QSpacerItem(20,20,QSizePolicy::Expanding);
	m_topTaskBtn = new QPushButton(this);
	m_topTaskBtn->setObjectName("toppageBtn");
	m_topTaskBtn->setFixedSize(90, 40);
	m_topTaskBtn->setText("首页");
	m_addTaskBtn = new QPushButton(this);
	m_addTaskBtn->setObjectName("addtaskBtn");
	m_addTaskBtn->setFixedSize(32, 32);
	m_hblayout->addWidget(m_topTaskBtn);
	m_hblayout->addWidget(m_addTaskBtn);
	m_hblayout->setContentsMargins(3, 10, 0, 0);
	m_topTaskBtn->setCheckable(true);
	m_taskGroup = new QButtonGroup;
	m_taskGroup->addButton(m_topTaskBtn);
	m_taskGroup->setExclusive(true);
	m_hblayout->setSpacing(1);
	m_hblayout->addSpacerItem(m_spacerItem);
	this->setLayout(m_hblayout);
}

void TaskBarWidget::initSignals()
{
	connect(m_addTaskBtn, &QPushButton::clicked, this, &TaskBarWidget::addNewBtn);
	connect(m_taskGroup, SIGNAL(buttonClicked(int)), this, SLOT(setSelectWorkStation(int)));
	connect(m_topTaskBtn, &QPushButton::clicked, this, &TaskBarWidget::clickedToppage);
}

void TaskBarWidget::addNewBtn()
{
	m_hblayout->removeWidget(m_topTaskBtn);
	m_hblayout->removeWidget(m_addTaskBtn);
	m_hblayout->removeItem(m_spacerItem);
	for (auto it = m_taskBtnList.begin(); it != m_taskBtnList.end()-1&&(*it)!=nullptr; it++)
	{
		m_hblayout->removeWidget(*it);
	}
	if (m_taskBtnList.size() < 8) 
	{
		countnum++;
		m_taskBtn = new TaskButton(QString("%1").arg(countnum),countnum, this);
		emit sendCreatNewWorkSation(countnum);
		if (m_taskBtn != nullptr) 
		{
			m_taskBtn->setFixedSize(150, 40);
			m_taskBtn->setCheckable(true);
			m_taskGroup->addButton(m_taskBtn,countnum);
			m_taskBtnList.append(m_taskBtn);
			setSelectWorkStation(countnum);
			connect(m_taskBtn, &TaskButton::sendDeleteBtn, this, &TaskBarWidget::deleteTaskBtn);
			connect(m_taskBtn, &TaskButton::sendDeletesignlas, this, &TaskBarWidget::deleteWorkStation);
		}
		m_taskBtn = nullptr;
	}
	m_hblayout->addWidget(m_topTaskBtn);
	for (auto it = m_taskBtnList.begin(); it != m_taskBtnList.end(); it++)
	{
		m_hblayout->addWidget(*it);	
	}
	m_taskBtnList[m_taskBtnList.size() - 1]->setChecked(true);
	m_hblayout->addWidget(m_addTaskBtn);
	m_hblayout->addSpacerItem(m_spacerItem);
	this->setLayout(m_hblayout);
	update();
}

