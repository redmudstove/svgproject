﻿#ifndef SETTINGWIDGET_H_
#define SETTINGWIDGET_H_
//这个页面是关闭，最大化，最小化的页面
#pragma execution_character_set("utf-8")
#include "components/controlclass/taskbutton/taskbutton.h"
#include <QWidget>
class QPushButton;
class QHBoxLayout;
class SettingWidget:public QWidget
{
	Q_OBJECT
public:
	SettingWidget(QWidget* parent = nullptr);
	~SettingWidget();
protected:
	void paintEvent(QPaintEvent* event) override;
	bool eventFilter(QObject* watched, QEvent* event) override;
private:
	void initWidget();
	void initSignals();
signals:
	void sendClose();
	void sendStates(int states);
	void sendMinum();
private:
	QPushButton* m_closeBtn = nullptr;
	QPushButton* m_mimumBtn = nullptr;
	QPushButton* m_maxumBtn = nullptr;
	QHBoxLayout* m_hb = nullptr;
	QPushButton* m_topageBtn = nullptr;
	TaskButton* m_taskbtn = nullptr;
private:
	int m_windowsStats;
};





#endif
