﻿#ifndef TASKBARWIDGET_H_
#define TASKBARWIDGET_H_
#include "components/controlclass/taskbutton/taskbutton.h"
#include <QWidget>
#include <QVector>
class QPushButton;
class QHBoxLayout;
class QSpacerItem;
#include <QList>
#pragma execution_character_set("utf-8")
class TaskBarWidget:public QWidget
{
	Q_OBJECT
public:
	TaskBarWidget(QWidget* parent = nullptr);
	~TaskBarWidget();
public slots:
	void clickedToppage();
	void deleteTaskBtn();
	void creatNewTaskBtn();
	void deleteWorkStation(const int& index);
	void setSelectWorkStation(const int&id);
	void taskButtonClicked(const int& index);
	void reciviedCreateSignals();
signals:
	void sendTopPageClicked();
	void sendCreatTaskBtn();
	void sendCreatNewWorkSation(const int& index);
	void sendDeleteWorkSation(const int& index);
	void sendSelectWorkSation(const int& index);
protected:
	void paintEvent(QPaintEvent* event) override;

private:
	void initWidget();
	void initSignals();
	void addNewBtn();
private:
	QPushButton* m_addTaskBtn = nullptr;
	QPushButton* m_topTaskBtn = nullptr;
	QHBoxLayout* m_hblayout = nullptr;
	TaskButton* m_taskBtn = nullptr;
	QList<TaskButton*> m_taskBtnList;
	QButtonGroup* m_taskGroup = nullptr;
	TaskButton* m_btn = nullptr;
	static int countnum;
	QSpacerItem* m_spacerItem = nullptr;

};

#endif
