﻿
#include "components/controlwidget/settingwidget.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QPaintEvent>
#include <QStyleOption>
#include <QPainter>
#include <QButtonGroup>
#include <QDebug>
SettingWidget::SettingWidget(QWidget* parent /*= nullptr*/):QWidget(parent),m_windowsStats(3)
{
	initWidget();
	initSignals();
	m_closeBtn->installEventFilter(this);
	m_maxumBtn->installEventFilter(this);
	m_mimumBtn->installEventFilter(this); 
	
}


SettingWidget::~SettingWidget()
{

}

void SettingWidget::paintEvent(QPaintEvent* event)
{

	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

bool SettingWidget::eventFilter(QObject* watched, QEvent* event)
{
	if(watched==m_closeBtn||watched==m_maxumBtn||watched==m_mimumBtn)
	{
		if (event->type() == QEvent::KeyPress) 
		{
			return true;
		}
		
	}
	return QWidget::eventFilter(watched, event);
	
}

void SettingWidget::initWidget()
{
	m_hb = new QHBoxLayout;

	m_hb->addStretch();
	m_closeBtn = new QPushButton(this);
	m_closeBtn->setObjectName("closeBtn");
	m_closeBtn->setFixedSize(32, 48);
	m_mimumBtn = new QPushButton(this);
	m_mimumBtn->setObjectName("minimBtn");
	m_mimumBtn->setFixedSize(32, 48);
	m_maxumBtn = new QPushButton(this);
	m_maxumBtn->setObjectName("maxumBtn");
	m_maxumBtn->setFixedSize(32, 48);

	m_maxumBtn->setCheckable(true);
	m_closeBtn->setCheckable(true);
	m_mimumBtn->setCheckable(true);
	m_hb->addStretch();
	m_hb->addWidget(m_mimumBtn);
	m_hb->addWidget(m_maxumBtn);
	m_hb->addWidget(m_closeBtn);
	m_hb->setSpacing(0);
	m_hb->setContentsMargins(0, 0, 0, 0);
	this->setLayout(m_hb);

}

void SettingWidget::initSignals()
{
	connect(m_closeBtn, &QPushButton::clicked, this, [=]() {
		emit sendClose();
		});
	connect(m_mimumBtn, &QPushButton::clicked, this, [=]() {
		emit sendMinum();
		});
	connect(m_maxumBtn, &QPushButton::clicked, this, [=]() {

		if (m_windowsStats == 3) {
			emit sendStates(1);
			m_windowsStats = 1;
		}

		else if (m_windowsStats == 1) {
			emit sendStates(3);
			m_windowsStats = 3;
		}
		});

}

