﻿#include "components/controlwidget/topcreatfilewidget.h"
#include <QHBoxLayout>
#include "components/controlclass/createsvgbutton/createsvgbutton.h"
#include <QDebug>
#pragma execution_character_set("utf-8")
TopCreateFilesWidget::TopCreateFilesWidget(QWidget* parent /*= nullptr*/) :QWidget(parent)
{
	initWidget();
	iniSignals();
}

TopCreateFilesWidget::~TopCreateFilesWidget()
{

}

void TopCreateFilesWidget::initWidget()
{
	m_hb = new QHBoxLayout;
	m_svgBtn = new CreateSvgButton("新建SVG文件", this);
	m_svgBtn->setFixedSize(210, 250);
	m_pdfBtn = new CreateSvgButton("新建PDF文件", this);
	m_pdfBtn->setFixedSize(210, 250);
	m_xmindBtn = new CreateSvgButton("新建Xmind文件", this);
	m_xmindBtn->setFixedSize(210, 250);
	m_editBtn = new CreateSvgButton("编辑器新建svg",this);
	m_editBtn->setFixedSize(210, 250);
	m_testBTn = new CreateSvgButton("测试",this);
	m_testBTn->setFixedSize(210, 250);
	m_hb->addWidget(m_svgBtn);
	m_hb->addWidget(m_pdfBtn);
	m_hb->addWidget(m_xmindBtn);
	m_hb->addWidget(m_editBtn);
	m_hb->addWidget(m_testBTn);
	this->setLayout(m_hb);
}

void TopCreateFilesWidget::iniSignals()
{
	connect(m_svgBtn, &CreateSvgButton::sendCreateNewWorkStation, this, [=]() {
		emit sendCreatedSvgCanvas();
		qDebug() << "in createsvgbutton: clicked";
		});
}
