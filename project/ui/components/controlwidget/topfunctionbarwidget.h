#ifndef TOPFUNCTIONBARWIDGET_H_
#define TOPFUNCTIONBARWIDGET_H_
#include <QWidget>
#pragma execution_character_set("utf-8")
class QPushButton;
class QVBoxLayout;
class QButtonGroup;
class TopFunctionBarWidget : public QWidget
{
	Q_OBJECT
public:
	TopFunctionBarWidget(QWidget* parent =nullptr);
	~TopFunctionBarWidget();
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initSignals();

signals:
	void sendOpenCreatfilesWidget();
	void sendOpenFilesWidget();
	void sendRecyleBinWidget();
	void sendWaitforDealWidget();

private:
	QVBoxLayout* m_vb = nullptr;
	QPushButton* m_creatFileBtn = nullptr;
	QPushButton* m_openFile = nullptr;
	QPushButton* m_recylebinBtn = nullptr;
	QPushButton* m_waifordealBtn = nullptr;
	QPushButton* m_tooboxBtn = nullptr;
	QButtonGroup* m_groupBtn = nullptr;

};






#endif