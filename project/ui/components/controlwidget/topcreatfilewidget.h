﻿#ifndef TOPCREATEFILEWIDGET_H_
#define TOPCREATEFILEWIDGET_H_
#include <QWidget>
class CreateSvgButton;
class QHBoxLayout;
/// <summary>
/// TopPageWidget:
/// 首页,放置三个创建不同文件的按钮
/// </summary>
class TopCreateFilesWidget :public QWidget
{
	Q_OBJECT
public:
	TopCreateFilesWidget(QWidget* parent = nullptr);
	~TopCreateFilesWidget();
signals:
	void sendCreatedSvgCanvas();
protected:
private:
	void initWidget();
	void iniSignals();
private:
	CreateSvgButton* m_svgBtn;
	CreateSvgButton* m_pdfBtn = nullptr;;
	CreateSvgButton* m_xmindBtn = nullptr;
	CreateSvgButton* m_editBtn = nullptr;
	CreateSvgButton* m_testBTn = nullptr;
	QHBoxLayout* m_hb = nullptr;


};

#endif 