﻿#ifndef TOPPAGEWIDGET_H_
#define TOPPAGEWIDGET_H_
#include <QWidget>
#include "components/controlwidget/topfunctionbarwidget.h"
class TopCreateFilesWidget;
class QHBoxLayout;
class QStackedWidget;
class TopPageWidget :public QWidget
{
	Q_OBJECT
public:
	TopPageWidget(QWidget* parent = nullptr);
	~TopPageWidget();

signals:
	void sendCreatedSvgCanvas();
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initStackedWidget();
	void initSignals();
private:
	TopCreateFilesWidget* m_createfiles = nullptr;
	TopFunctionBarWidget* m_topfunctionwidget = nullptr;
	QStackedWidget* m_stackwidget = nullptr;
	QHBoxLayout* m_hb = nullptr;


};




#endif
