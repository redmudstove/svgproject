﻿#ifndef TOPTASKBARWIDGET_H_
#define TOPTASKBARWIDGET_H_
#include "components/controlwidget/settingwidget.h"
#include "components/controlwidget/taskbarwidget.h"
#include <QWidget>
class QHBoxLayout;
class TopTaskBarWidget:public QWidget	
{
	Q_OBJECT
public:
	TopTaskBarWidget(QWidget* parent = nullptr);
	~TopTaskBarWidget();
public slots:
	void reciviedCreateSignals();
signals:
	void sendCreatNewWorkSation(const int& index);
	void sendDeleteWorkStation(const int&index);
	void sendSelectWorkSation(const int& index);
	void sendTopPageClicked();
	void sendToTaskBarToCreateSignals();
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	void initWidget();
	void initSignals();
public:
	QHBoxLayout* m_hbLayout = nullptr;
	SettingWidget* m_settingwidget = nullptr;
	TaskBarWidget* m_taskbarWidget = nullptr;
};


#endif
