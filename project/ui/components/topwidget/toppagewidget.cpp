﻿#include "components/topwidget/toppagewidget.h"
//#include "components/"
#include <QHBoxLayout>
#include <QStackedWidget>
#include "components/controlwidget/topcreatfilewidget.h"
#include <QPushButton>
#include <QPaintEvent>
#include <QDebug>
TopPageWidget::TopPageWidget(QWidget* parent /*= nullptr*/)
{
	initWidget();
	initStackedWidget();
	initSignals();
	m_hb->setContentsMargins(0, 0, 0, 0);
	this->setLayout(m_hb);
}

TopPageWidget::~TopPageWidget()
{
	
}

void TopPageWidget::paintEvent(QPaintEvent* event)
{

}

void TopPageWidget::initWidget()
{ 
	m_hb = new QHBoxLayout;
	m_topfunctionwidget = new TopFunctionBarWidget(this);
	m_topfunctionwidget->setMinimumWidth(160);
	//m_createfiles = new TopCreateFilesWidget(this);
	m_hb->addWidget(m_topfunctionwidget);
	//m_hb->addWidget(m_stackwidget);
	
	
}

void TopPageWidget::initStackedWidget()
{
	m_stackwidget = new QStackedWidget;
	QHBoxLayout* hb1 = new QHBoxLayout;
	m_createfiles = new TopCreateFilesWidget(m_stackwidget);

	hb1->addWidget(m_createfiles);

	QPushButton* m_test1 = new QPushButton("test1",m_stackwidget);
	m_stackwidget->insertWidget(0, m_createfiles);
	m_stackwidget->insertWidget(1, m_test1);
	m_test1->setFixedSize(100, 100);
	m_hb->addWidget(m_stackwidget);
	
}

void TopPageWidget::initSignals()
{
	connect(m_topfunctionwidget, &TopFunctionBarWidget::sendOpenCreatfilesWidget, this, [=]() {
		m_stackwidget->setCurrentIndex(0);
		});
	connect(m_topfunctionwidget, &TopFunctionBarWidget::sendOpenFilesWidget, this, [=]() {
		m_stackwidget->setCurrentIndex(1);
		});
	connect(m_createfiles, &TopCreateFilesWidget::sendCreatedSvgCanvas, this, [=]() {
		emit sendCreatedSvgCanvas();
		});
}

