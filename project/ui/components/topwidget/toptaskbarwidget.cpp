﻿#include "components/topwidget/toptaskbarwidget.h"
#include <QStyleOption>
#include <QPainter>
#include <QPen>
#include <QColor>
#include <QPaintEvent>
#include <QHBoxLayout>
TopTaskBarWidget::TopTaskBarWidget(QWidget* parent /*= nullptr*/):QWidget(parent)
{
	initWidget();
	initSignals();
}

TopTaskBarWidget::~TopTaskBarWidget()
{

}

void TopTaskBarWidget::reciviedCreateSignals()
{
	emit sendToTaskBarToCreateSignals();
}

void TopTaskBarWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void TopTaskBarWidget::initWidget()
{
	m_hbLayout = new QHBoxLayout;
	m_settingwidget = new SettingWidget(this);
	m_taskbarWidget = new TaskBarWidget(this);
	//m_settingwidget->setFixedWidth(98);
	m_hbLayout->addWidget(m_taskbarWidget);
	m_hbLayout->addStretch();
	m_hbLayout->addWidget(m_settingwidget);
	m_hbLayout->setContentsMargins(0, 0, 0, 0);
	this->setLayout(m_hbLayout);
}

void TopTaskBarWidget::initSignals()
{
	connect(m_taskbarWidget,&TaskBarWidget::sendCreatNewWorkSation, this, [=](const int& index) {
		emit sendCreatNewWorkSation(index);
		});
	connect(m_taskbarWidget, &TaskBarWidget::sendSelectWorkSation, this, [=](const int& index) {
		emit sendSelectWorkSation(index);
		});
	connect(m_taskbarWidget, &TaskBarWidget::sendDeleteWorkSation, this, [=](const int&index) {
		emit sendDeleteWorkStation(index);
		});
	
	connect(m_taskbarWidget, &TaskBarWidget::sendTopPageClicked, this, [=]() {
		emit sendTopPageClicked();
		});

	connect(this, &TopTaskBarWidget::sendToTaskBarToCreateSignals, m_taskbarWidget, &TaskBarWidget::reciviedCreateSignals);
}

