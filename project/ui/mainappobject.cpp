﻿#include "ui/mainapplication.h"
#include "ui/mainappobject.h"
#include "components/startloadpage/opensplash.h"
MainAppObject::MainAppObject(QObject* parent /*= nullptr*/):QObject(parent)
{

}

MainAppObject::~MainAppObject()
{
	delete m_mainApplication;
}

void MainAppObject::InitMainApplication()
{
	OpenSplash::getInstance()->setStagePercent(0, tr("初始化 ..."));
	m_mainApplication = new MainApplication(nullptr);
	OpenSplash::getInstance()->setStart(m_mainApplication, tr("RedMudStove"), QString(""));
	OpenSplash::getInstance()->setStagePercent(40, tr("初始化主界面 ..."));
	OpenSplash::getInstance()->setStagePercent(80, tr("加载界面 ..."));
	OpenSplash::getInstance()->setStagePercent(100, tr("加载完毕 ..."));
	OpenSplash::getInstance()->setFinish();

	m_mainApplication->showNormal();
}

