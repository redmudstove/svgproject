#ifndef MAINAPPOBJECT_H_
#define MAINAPPOBJECT_H_
#include <QObject>
class MainApplication;
class MainAppObject:public QObject
{
	Q_OBJECT
public:
	explicit MainAppObject(QObject* parent = nullptr);
	~MainAppObject();
public:
	void InitMainApplication();
protected:

private:
	MainApplication* m_mainApplication = nullptr;
};
#endif
