﻿#include "ui/mainapplication.h"
#include <QRect>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QFile>
#include <QDebug>

MainApplication::MainApplication(QWidget* parent /*= nullptr*/):QWidget(parent),
	m_bPressed(false), m_curIndex(0)
{
	setWidgetProperties();
	initQssFile();
	initWidget();
	initSignals();
	
}

MainApplication::~MainApplication()
{

}

void MainApplication::setAreaMovable(const QRect rt)
{
	if (m_areaMovable != rt) {
		m_areaMovable = rt;
	}
}


void MainApplication::changeStates(int states)
{
	if (states == 1) {
		this->showMaximized();
	}
	else if (states == 3)
	{
		this->showNormal();
	}
}

void MainApplication::mouseMoveEvent(QMouseEvent* event)
{
	if (m_bPressed)
	{
		move(event->globalPos() - m_ptPress);
	}
}

void MainApplication::mousePressEvent(QMouseEvent* event)
{
	if ((event->button() == Qt::LeftButton)) {
		m_bPressed = true;
		m_ptPress = event->globalPos() - this->pos();
	}
}

void MainApplication::mouseReleaseEvent(QMouseEvent* event)
{
	m_bPressed = false;
}

void MainApplication::paintEvent(QPaintEvent* event)
{

}

void MainApplication::setChangedtoToppage()
{
	
	if(m_curIndex!=0){
		m_vb->removeWidget(m_setting);
		m_vb->removeWidget(m_worklist[m_curIndex]);
		m_worklist[m_curIndex]->hide();
		m_curIndex = 0;
		m_vb->addWidget(m_setting);
		m_worklist[m_curIndex]->show();
		m_vb->addWidget(m_worklist[m_curIndex]);
	}
	

}

void MainApplication::deleteWorkStation(const int& index)
{
	for (int i = 1; i <= m_worklist.size()-1; ++i) {
		SvgWorkStationWidget* temp = static_cast<SvgWorkStationWidget*>(m_worklist[i]);
		if (temp->getIndex() == index) {
			// 如果删除的是正在展示的画面,将其移除布局中
			if (m_curIndex == i) {
				m_worklist[i]->hide();
				m_vb->removeWidget(m_setting);
				m_vb->removeWidget(m_worklist[i]);
				m_worklist.removeOne(m_worklist[i]);
				delete temp;
				m_curIndex = m_worklist.size() - 1;
				m_vb->addWidget(m_setting);
				m_vb->addWidget(m_worklist[m_curIndex]);
				m_worklist[m_curIndex]->show();
				temp = nullptr;
			}
			else
			{
				SvgWorkStationWidget* temp1 = static_cast<SvgWorkStationWidget*>(m_worklist[m_curIndex]);
				int index = temp->getIndex();
				m_vb->removeWidget(m_worklist[m_curIndex]);
				m_worklist.removeOne(m_worklist[i]);
				m_curIndex = m_worklist.size() - 1;
				delete temp;

				m_vb->addWidget(m_worklist[m_curIndex]);
				m_worklist[m_curIndex]->show();
				temp1 = nullptr;
				temp = nullptr;
			}
			break;
		}
		temp = nullptr;
	}
}

void MainApplication::createNewWorkStation(const int& index)
{
	//创建新的工作区
	//qDebug() << "recived new signals";
	SvgWorkStationWidget* workstation = new SvgWorkStationWidget(index, this);
	//放入容器的尾部
	m_worklist.push_back(workstation);
	//并将其加入布局中
	//将此时正在展示的页面隐藏。并移除布局中
	m_vb->removeWidget(m_setting);
	m_vb->removeWidget(m_worklist[m_curIndex]);
	//将此时的下标更改为队尾的坐标
	m_worklist[m_curIndex]->hide();
	m_curIndex = m_worklist.size() - 1;
	m_vb->addWidget(m_setting);
	m_vb->addWidget(m_worklist[m_curIndex]);
	m_worklist[m_curIndex]->show();
	//setSelectWorkStation(workstation->getIndex());
	workstation = nullptr;
	update();

	//SvgWorkStationWidget* workstation = new SvgWorkStationWidget(index,this);
	//m_worklist.push_back(workstation);
	//if (m_curIndex != 0) {
	//	m_vb->removeWidget(m_worklist[m_curIndex]);
	//}


	//for (int i = 1; i <= m_worklist.size() - 1; i++) {
	//	m_worklist[i]->hide();
	//}
	//m_curIndex = m_worklist.size() - 1;

	////m_vb->addWidget(m_setting);
	//m_vb->addWidget(m_worklist[m_curIndex]);
	//m_worklist[m_curIndex]->show();
	//workstation = nullptr;
	//update();
}

void MainApplication::setSelectWorkStation(const int& index)
{
	//传递过来的是
	for (int i = 1; i <= m_worklist.size()-1; i++) {
		SvgWorkStationWidget* temp = static_cast<SvgWorkStationWidget*>(m_worklist[i]);
		//得到此时的所点击的工作区，如果此时的i与所展示的是同一个，则不做操作。
		if (temp->getIndex() == index) {
			m_vb->removeWidget(m_setting);
			m_vb->removeWidget(m_worklist[m_curIndex]);
			m_worklist[m_curIndex]->hide();
			m_curIndex = i;
			m_vb->addWidget(m_setting);
			m_vb->addWidget(m_worklist[m_curIndex]);
			m_worklist[m_curIndex]->show();
		}
		else
		{
			temp->hide();
		}
		temp = nullptr;
	}
	update();
}

void MainApplication::setWidgetProperties()
{
	this->resize(1490, 900);
	this->setWindowFlags(Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_StyledBackground, true);
	m_areaMovable = geometry();
	m_bPressed = false;
}

void MainApplication::initQssFile()
{
	QFile file(":/qss/res/main.qss");
	file.open(QFile::ReadOnly);
	this->setStyleSheet(file.readAll());
	file.close();
}

void MainApplication::initWidget()
{
	m_setting = new TopTaskBarWidget(this);
	m_setting->setFixedHeight(50);
	m_fuctionbar = new TopPageWidget(this);
	m_worklist.push_back(m_fuctionbar);
	m_vb = new QVBoxLayout(this);
	m_vb->addWidget(m_setting);
	m_vb->addWidget(m_worklist[m_curIndex]);
	m_vb->setSpacing(0);
	m_vb->setContentsMargins(0, 0, 0, 0);
	
}

void MainApplication::initSignals()
{
	connect(m_setting->m_settingwidget, &SettingWidget::sendClose, this, &QWidget::close);
	connect(m_setting->m_settingwidget, &SettingWidget::sendStates, this, &MainApplication::changeStates);
	connect(m_setting->m_settingwidget, &SettingWidget::sendMinum, this, &MainApplication::showMinimized);

	connect(m_fuctionbar, &TopPageWidget::sendCreatedSvgCanvas, m_setting, &TopTaskBarWidget::reciviedCreateSignals);

	connect(m_setting, &TopTaskBarWidget::sendSelectWorkSation, this, &MainApplication::setSelectWorkStation);
	connect(m_setting, &TopTaskBarWidget::sendCreatNewWorkSation, this, &MainApplication::createNewWorkStation);

	connect(m_setting, &TopTaskBarWidget::sendDeleteWorkStation, this, &MainApplication::deleteWorkStation);

	connect(m_setting, &TopTaskBarWidget::sendTopPageClicked, this, &MainApplication::setChangedtoToppage);
}

