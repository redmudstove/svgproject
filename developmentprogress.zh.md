### SVG编辑器进度
#### 一. 画布(核心)
##### 功能表格


| 名称 | 模块描述 | 实现情况 |备注 |
|---|---|---|---|
|[1. 画布移动](#1-画布移动) | 按下空格键的同时点击鼠标的左键能够移动画布 |<center><font color=red>X</font></center>||
|2. 外围矩形能够显示当前的大小 ||<center><font color=red>X</font></center>||
|3. 绘制直线 | 能够绘制直线 | <center><font color=red>X</font></center>| |
|4. 绘制矩形||<center><font color=red>X</font></center>||
|5. 绘制圆形||<center><font color=red>X</font></center>||
|6. 绘制椭圆||<center><font color=red>X</font></center>||
|7. 绘制特殊图形||<center><font color=red>X</font></center>||
|6. 绘制自由线|其中是否支持贝塞尔曲线|<center><font color=red>X</font></center>||
|7. 绘制字体||<center><font color=red>X</font></center>||

##### 功能点
###### 1. 画布移动
##### 过程中需要改进的地方

- [ ] <font color =red>打开标尺的栏需要过滤掉空格按钮</font>
- [ ] 

